<?php
namespace Block\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * TranslatesFixture
 *
 */
class TranslatesFixture extends TestFixture {

/**
 * Table name
 *
 * @var string
 */
	public $table = 'i18n';

/**
 * Fields
 *
 * @var array
 */
	public $fields = [
		'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
		'locale' => ['type' => 'string', 'length' => 5, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
		'model' => ['type' => 'string', 'length' => 64, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
		'foreign_key' => ['type' => 'string', 'length' => 36, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
		'field' => ['type' => 'string', 'length' => 64, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
		'content' => ['type' => 'text', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
		'created' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
		'modified' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
		'_indexes' => [
			// 'model' => ['type' => 'index', 'columns' => ['model', 'foreign_key', 'field'], 'length' => []],
		],
		'_constraints' => [
			'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
		],
		'_options' => ['engine' => 'InnoDB', 'collation' => 'utf8_general_ci'],
	];

/**
 * Records
 *
 * @var array
 */
	public $records = [
		['locale' => 'spa', 'model' => 'Contents', 'foreign_key' => 1, 'field' => 'title', 'content' => 'Contenido 1'],
    ['locale' => 'eng', 'model' => 'Contents', 'foreign_key' => 1, 'field' => 'title', 'content' => 'Content 1'],
		['locale' => 'spa', 'model' => 'Blocks', 'foreign_key' => 111, 'field' => 'body', 'content' => 'Bloque 1'],
    ['locale' => 'eng', 'model' => 'Blocks', 'foreign_key' => 111, 'field' => 'body', 'content' => 'Block 1'],
    ['locale' => 'spa', 'model' => 'Blocks', 'foreign_key' => 112, 'field' => 'body', 'content' => 'Bloque 2'],
    ['locale' => 'eng', 'model' => 'Blocks', 'foreign_key' => 112, 'field' => 'body', 'content' => 'Block 2'],
    ['locale' => 'spa', 'model' => 'Blocks', 'foreign_key' => 113, 'field' => 'body', 'content' => 'Bloque 3'],
    ['locale' => 'eng', 'model' => 'Blocks', 'foreign_key' => 113, 'field' => 'body', 'content' => 'Block 3'],
	];

}
