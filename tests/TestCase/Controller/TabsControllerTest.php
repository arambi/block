<?php
namespace Block\Test\TestCase\Controller;

use Block\Controller\TabsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * Block\Controller\TabsController Test Case
 */
class TabsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.block.tabs'
    ];

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
