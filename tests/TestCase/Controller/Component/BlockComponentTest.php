<?php
namespace Block\Test\TestCase\Controller\Component;

use Block\Controller\Component\BlockComponent;
use Cake\Controller\ComponentRegistry;
use Cake\TestSuite\TestCase;

/**
 * Block\Controller\Component\BlockComponent Test Case
 */
class BlockComponentTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Block\Controller\Component\BlockComponent
     */
    public $Block;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $registry = new ComponentRegistry();
        $this->Block = new BlockComponent($registry);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Block);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
