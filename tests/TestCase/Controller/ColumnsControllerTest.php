<?php
namespace Block\Test\TestCase\Controller;

use Block\Controller\ColumnsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * Block\Controller\ColumnsController Test Case
 */
class ColumnsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.block.columns'
    ];

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
