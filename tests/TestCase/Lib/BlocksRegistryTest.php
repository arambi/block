<?php
namespace Block\Test\TestCase\Lib;

use Block\Lib\BlocksRegistry;
use Cake\TestSuite\TestCase;

/**
 * Manager\Controller\Component\CrudToolComponent Test Case
 */
class BlocksRegistryTest extends TestCase {

/**
 * setUp method
 *
 * @return void
 */
  public function setUp() {
    parent::setUp();
   
  }

/**
 * tearDown method
 *
 * @return void
 */
  public function tearDown() {
    parent::tearDown();
  }

/**
 * Comprueba que añade correctamente bloques y que los toma correctamente
 */
  public function testAddAndGet()
  {
    $data = [
        'title' => __d( 'admin', 'Test'),
        'icon' => 'fa fa-test',
        'afterAddTarget' => 'parent',
        'inline' => true,
        'unique' => false
    ];

    BlocksRegistry::add( 'test', $data);

    $block = BlocksRegistry::get( 'test');
    $this->assertEquals( 'Test', $block ['title']);
    $this->assertEquals( 'parent', $block ['afterAddTarget']);
    $this->assertEquals( 'test', $block ['key']);
    
    $title = BlocksRegistry::get( 'test', 'title');
    $this->assertEquals( $title, 'Test');
  }

/**
 * Comprueba el método BlocksRegistry::getAvailables
 */
  public function testGetAvailables()
  {
    BlocksRegistry::availables([
      'Posts' => [
          'text', 'gallery'
      ],
      'Wrappers' => [
          'text', 'gallery', 'content'
      ]
    ]);

    $blocks = BlocksRegistry::getAvailables( 'Posts');
    $this->assertEquals( $blocks ['text']['key'], 'text');
    $this->assertEquals( $blocks ['gallery']['key'], 'gallery');
  }

/**
 * Comprueba el método BlocksRegistry::getMany
 */
  public function testGetMany()
  {
    $blocks = BlocksRegistry::getMany( ['gallery', 'text']);
    $this->assertTrue( array_key_exists( 'gallery', $blocks));
    $this->assertTrue( array_key_exists( 'text', $blocks));
    $this->assertTrue( is_array( $blocks ['text']));
    $this->assertTrue( is_array( $blocks ['gallery']));
  }
}
