<?php
namespace Block\Test\TestCase\Model\Table;

use Block\Model\Table\TabsTable;
use Manager\TestSuite\CrudTestCase;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * Block\Model\Table\TabsTable Test Case
 */
class TabsTableTest extends CrudTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.block.tabs'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Tabs') ? [] : ['className' => 'Block\Model\Table\TabsTable'];
        $this->Tabs = TableRegistry::get('Tabs', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Tabs);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
          
      public function testConfig()
      {
        $this->assertCrudDataEdit( 'update', $this->Tabs);
        $this->assertCrudDataIndex( 'index', $this->Tabs);
      }
}
