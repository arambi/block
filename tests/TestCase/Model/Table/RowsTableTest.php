<?php
namespace Block\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Block\Model\Table\RowsTable;
use Cake\TestSuite\TestCase;

/**
 * Block\Model\Table\RowsTable Test Case
 */
class RowsTableTest extends TestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = [
		'Rows' => 'plugin.block.rows', 
		'Contents' => 'plugin.block.contents'
	];

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$config = TableRegistry::exists('Rows') ? [] : ['className' => 'Block\Model\Table\RowsTable'];

		$this->Rows = TableRegistry::get('Rows', $config);

	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Rows);

		parent::tearDown();
	}

	public function testEmpty()
	{

	}

}
