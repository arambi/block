<?php
namespace Block\Test\TestCase\Model\Table;

use Block\Model\Table\ColumnsTable;
use Manager\TestSuite\CrudTestCase;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * Block\Model\Table\ColumnsTable Test Case
 */
class ColumnsTableTest extends CrudTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.block.columns'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Columns') ? [] : ['className' => 'Block\Model\Table\ColumnsTable'];
        $this->Columns = TableRegistry::get('Columns', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Columns);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
          
      public function testConfig()
      {
        $this->assertCrudDataEdit( 'update', $this->Columns);
        $this->assertCrudDataIndex( 'index', $this->Columns);
      }
}
