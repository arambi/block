<?php
namespace Block\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Block\Model\Table\BlocksTable;
use Cake\TestSuite\TestCase;

/**
 * Block\Model\Table\BlocksTable Test Case
 */
class BlocksTableTest extends TestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = [
		'Blocks' => 'plugin.block.blocks', 

	];

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$config = TableRegistry::exists('Blocks') ? [] : ['className' => 'Block\Model\Table\BlocksTable'];

		$this->Blocks = TableRegistry::get('Blocks', $config);

	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Blocks);

		parent::tearDown();
	}

/**
 * Verifica que el tipo de bloque esté bien seteado por beforeFind()
 */
	public function testSetType()
	{
		$block = $this->Blocks->find()->first();

		$this->assertTrue( $block->has( 'block_type'));
		$this->assertTrue( is_array( $block->block_type));
	}
}
