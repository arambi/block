<?php
namespace Block\Test\TestCase\Model\Behavior;

use Cake\Datasource\ConnectionManager;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\TestSuite\TestCase;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use I18n\Lib\Lang;
use Cake\ORM\Entity;
use Cake\ORM\Behavior\Translate\TranslateTrait;
use Manager\Model\Entity\CrudEntityTrait;
use Cake\I18n\I18n;
use Block\Lib\BlocksRegistry;
use Cake\Core\Plugin;
use Cake\Collection\Collection;


class ContentsTable extends Table 
{

	public function initialize(array $options) 
	{
		BlocksRegistry::availables([
			'Contents' => [
					'text', 'gallery'
			]
		]);

		$this->addBehavior( 'Manager.Crudable');
		$this->addBehavior( 'Block.Blockable', [
			'blocks' => [ 
				'text', 'gallery'
			],
			'defaults' => [
				'text'
			]
		]);
		$this->addBehavior( 'Cofree.Contentable');
		$this->addBehavior( Configure::read( 'I18n.behavior'), [
			'fields' => ['title']
		]);
		$this->crud->associations( ['Rows', 'Blocks']);
		$this->entityClass( 'Block\Test\TestCase\Model\Behavior\Content');
	}

}

class Content extends Entity 
{
  use CrudEntityTrait;
  use TranslateTrait;

/**
 * Fields that can be mass assigned using newEntity() or patchEntity().
 *
 * @var array
 */
	protected $_accessible = [
		'title' => true,
		'body' => true,
    'rows' => true
	];
}

/**
 * Block\Model\Behavior\BlockableBehavior Test Case
 */
class BlockableBehaviorTest extends TestCase 
{

	public $fixtures = [
		'plugin.block.contents',
		'plugin.i18n.languages',
    'plugin.block.rows',
		'plugin.block.columns',
		'plugin.block.translates',
    'plugin.manager.contents_translations',
	];

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() 
	{
		// Quita el plugin section si está leído
    if( Plugin::loaded( 'Section'))
    {
      Plugin::unload( 'Section');
    }

    // Quita el plugin Website si está leído
    if( Plugin::loaded( 'Website'))
    {
      Plugin::unload( 'Website');
    }

		parent::setUp();

		$this->connection = ConnectionManager::get('test');
		$this->Contents = new ContentsTable([
			'alias' => 'Contents',
			'table' => 'contents',
			'connection' => $this->connection
		]);
		$this->Languages = TableRegistry::get( 'Langs', ['table' => 'languages']);
		Plugin::unload( 'Section');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() 
	{
		TableRegistry::clear();
		unset( $this->Blockable);
		unset( $this->Contents);
		unset( $this->connection);
		unset( $this->Languages);
		parent::tearDown();
	}

	public function setLanguages()
	{
		Lang::set( $this->Languages->find()->all());
	}

	public function testConfigure()
	{
		$this->Contents->crud
      ->addFields([
        'title' => 'Título',
      ])->setName( [
        'singular' => __d( 'admin', 'Contenido'),
        'plural' => __d( 'admin', 'Contenidos'),
      ])->addView( 'create', [
        'columns' => [
          [
            'cols' => 8,
            'box' => [
              [
                'title' => 'Creación',
                'elements' => [
                  'title',
                ]
              ]
            ]
          ]
        ]
      ]);

    $data = $this->Contents->crud->serialize( 'create');

    $this->assertTrue( array_key_exists( 'blocks', $data));
    $this->assertTrue( array_key_exists( 'text', $data ['blocks']));
	}

  public function testAddRowAndBlock()
  {
    $this->setLanguages();
    I18n::locale( 'spa');
    
    $data = [
      'rows' => [
        [
          'columns' => [
            [
              'position' => 1,
              'cols' => 6,
              'blocks' => [
                [
                  'subtype' => 'text',
                  'position' => 1,
                  '_translations' => [
                    'spa' => [
                      'title' => 'Hola',
                      'body' => 'Mundo'
                    ]
                  ]
                ]
              ]
            ],
            [
              'position' => 2,
              'cols' => 6,
              'blocks' => [
                [
                  'subtype' => 'text',
                  'position' => 1,
                  '_translations' => [
                    'spa' => [
                      'title' => 'Adiós',
                      'body' => 'Amigo'
                    ]
                  ]
                ]
              ]
            ]
          ]
        ]
      ],
    ];

    $content = $this->Contents->getNewEntity( $data);
    $saved = $this->Contents->saveContent( $content);
    $content = $this->Contents->find( 'content')->where( ['Contents.id' => 1])->first();
    $content = $this->Contents->patchContent( $content, $array);
    $saved = $this->Contents->saveContent( $content);
    $array = $this->Contents->find( 'array')->where( ['Contents.id' => 1])->first();
    $this->assertEquals( ($numrows + 1), count( $array ['rows']));
  }
	

	public function testSave()
	{
		$this->setLanguages();
		I18n::locale( 'spa');
		$array = $this->Contents->find( 'array')->where( ['Contents.id' => 1])->first();

    $data = [
      '_translations' => [
        'spa' => [
          'title' => 'Título'
        ],
        'eng' => [
          'title' => 'Títle'
        ]
      ],
      'rows' => [
        [
          'position' => 2,
          'columns' => [
            [
              'blocks' => [
                [
                  'position' => 1,
                  'subtype' => 'text',
                  '_translations' => [
                    'spa' => [
                      'body' => 'Nuevo bloque'
                    ],
                    'eng' => [
                      'body' => 'New block'
                    ]
                  ]
                ]
              ]
            ]
          ]
        ]
      ]
    ];

    $entity = $this->Contents->getNewEntity( $data);
    $this->Contents->saveContent( $entity);

		$content = $this->Contents->find( 'content')->where( ['Contents.id' => $entity->id])->first();
    _d( $content->rows);

    $saved = $this->Contents->saveContent( $content);
    $array = $this->Contents->find( 'array')->where( ['Contents.id' => 1])->first();
    _d( $array ['rows']);
    $this->assertEquals( 'Nuevo bloque', $array ['rows'][1]['blocks'][0]['spa']['body']);
    $this->assertEquals( $array ['rows'][1]['blocks'][0]['eng']['body'], 'New block');


    $array ['rows'] = array_reverse( $array ['rows']);
    $array ['rows'][0]['position'] = 1;
  	$array ['rows'][1]['position'] = 2;

  	$block = $this->Contents->Rows->Blocks->find()->where(['Blocks.id' => 111])->first();

  	$content = $this->Contents->find( 'content')->where( ['Contents.id' => 1])->first();
  	$content = $this->Contents->patchContent( $content, $array);
  	$saved = $this->Contents->saveContent( $content);
    $array = $this->Contents->find( 'array')->where( ['Contents.id' => 1])->first();
    

    $block_expected = $this->Contents->Rows->Blocks->find()->where(['Blocks.id' => 111])->first();
    $this->assertEquals( $block_expected->body, $block->body);
	}

/**
 * Test para comprobar que se mueven dos filas y todo se guarda correctamente
 */
	public function testMoveRows()
	{
		$this->setLanguages();
		I18n::locale( 'spa');

		$array = $this->Contents->find( 'array')->where( ['Contents.id' => 1])->first();

		$array ['rows'] = array_reverse( $array ['rows']);
    $array ['rows'][0]['position'] = 1;
  	$array ['rows'][1]['position'] = 2;

  	$row_0_id = $array ['rows'][0]['id'];
    $row_1_id = $array ['rows'][1]['id'];


  	$block = $this->Contents->Rows->Blocks->find()->where(['Blocks.id' => 111])->first();

  	$content = $this->Contents->find( 'content')->where( ['Contents.id' => 1])->first();
  	$content = $this->Contents->patchContent( $content, $array);
  	$saved = $this->Contents->saveContent( $content);
    $array = $this->Contents->find( 'array')->where( ['Contents.id' => 1])->first();
    

    $block_expected = $this->Contents->Rows->Blocks->find()->where(['Blocks.id' => 111])->first();
    $this->assertEquals( $block_expected->body, $block->body);

    $row_0 = $this->Contents->Rows->find()->where(['Rows.id' => $row_0_id])->first();
    $row_1 = $this->Contents->Rows->find()->where(['Rows.id' => $row_1_id])->first();

    $this->assertEquals( 1, $row_0->position);
    $this->assertEquals( 2, $row_1->position);
	}

	public function testMoveBlocks()
	{
		$this->setLanguages();
		I18n::locale( 'spa');

		$array = $this->Contents->find( 'array')->where( ['Contents.id' => 1])->first();

		$array ['rows'][0]['blocks'] = array_reverse( $array ['rows'][0]['blocks']);
		$array ['rows'][0]['blocks'][0]['position'] = 1;
  	$array ['rows'][0]['blocks'][1]['position'] = 2;

  	$block_0_id = $array ['rows'][0]['blocks'][0]['id'];
    $block_1_id = $array ['rows'][0]['blocks'][1]['id'];


		$content = $this->Contents->find( 'content')->where( ['Contents.id' => 1])->first();
  	$content = $this->Contents->patchContent( $content, $array);
  	$saved = $this->Contents->saveContent( $content);
    $array = $this->Contents->find( 'array')->where( ['Contents.id' => 1])->first();

    $block_0 = $this->Contents->Rows->Blocks->find()->where(['Blocks.id' => $block_0_id])->first();
    $block_1 = $this->Contents->Rows->Blocks->find()->where(['Blocks.id' => $block_1_id])->first();

    $this->assertEquals( 1, $block_0->position);
    $this->assertEquals( 2, $block_1->position);
	}

  public function testMoveBlocksFromRows()
  {
    $this->setLanguages();
    I18n::locale( 'spa');

    $data = $this->Contents->find( 'array')->where( ['Contents.id' => 1])->first();

    $move_block_id = $data ['rows'][0]['blocks'][0]['id'];
    $row_id = $data ['rows'][1]['id'];

    $block = $data ['rows'][0]['blocks'][0];
    unset( $data ['rows'][0]['blocks'][0]);
    $data ['rows'][1]['blocks'][] = $block;
    $count = count( $data ['rows'][1]['blocks']);
    $content = $this->Contents->find( 'content')->where( ['Contents.id' => 1])->first();
    $content = $this->Contents->patchContent( $content, $data);
    $saved = $this->Contents->saveContent( $content);
    
    $block = $this->Contents->Rows->Blocks->find()->where (['id' => $move_block_id])->first();
    $this->assertEquals( $row_id, $block->category_id);
  }

	public function testNewEntity()
	{
		$this->setLanguages();
		I18n::locale( 'spa');

		$entity = $this->Contents->getNewEntity();
		$this->assertTrue( $entity->has( 'rows'));
		$this->assertTrue( $entity->rows [0]->has( 'blocks'));
		$this->assertEquals( 'text', $entity->rows [0]->blocks [0]->subtype);
	}


/**
 * Verifica que se borran correctamente los bloques y si una fila está vacía (no tiene bloques), se borra la fila.
 * ESTE TEST NO ES VALIDO
 */
	public function _testDeleteRows()
	{
		$this->setLanguages();
		I18n::locale( 'spa');
		$array = $this->Contents->find( 'array')->where( ['Contents.id' => 1])->first();
		$content = $this->Contents->find( 'content')->where( ['Contents.id' => 1])->first();

		unset( $array ['rows'][0]['blocks'][0]);
		unset( $array ['rows'][0]['blocks'][1]);

		$content = $this->Contents->patchContent( $content, $array);
    $saved = $this->Contents->saveContent( $content);
    $array = $this->Contents->find( 'array')->where( ['Contents.id' => 1])->first();
		$this->assertTrue( empty( $array ['rows']));
	}

/**
 * ESTE TEST NO ES VALIDO
 */
	public function _testDeleteRows2()
	{
		$this->setLanguages();
		I18n::locale( 'spa');
		$array = $this->Contents->find( 'array')->where( ['Contents.id' => 1])->first();
		$content = $this->Contents->find( 'content')->where( ['Contents.id' => 1])->first();
		unset( $array ['rows'][0]);
		// _d( $array);
		$content = $this->Contents->patchContent( $content, $array);
    $saved = $this->Contents->saveContent( $content);
    $array = $this->Contents->find( 'array')->where( ['Contents.id' => 1])->first();

    $this->assertTrue( empty( $array ['rows']));
	}
}
