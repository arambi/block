<?php
namespace Block\Test\TestCase\View\Helper;

use Block\View\Helper\BlocksHelper;
use Cake\TestSuite\TestCase;
use Cake\View\View;

/**
 * Block\View\Helper\BlocksHelper Test Case
 */
class BlocksHelperTest extends TestCase
{

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $view = new View();
        $this->Blocks = new BlocksHelper($view);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Blocks);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
