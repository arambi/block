<?php
namespace Block\Test\TestCase\View\Cell;

use Block\View\Cell\RowCell;
use Cake\TestSuite\TestCase;

/**
 * Block\View\Cell\RowCell Test Case
 */
class RowCellTest extends TestCase
{

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->request = $this->getMock('Cake\Network\Request');
        $this->response = $this->getMock('Cake\Network\Response');
        $this->Row = new RowCell($this->request, $this->response);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Row);

        parent::tearDown();
    }

    /**
     * Test display method
     *
     * @return void
     */
    public function testDisplay()
    {
    }
}
