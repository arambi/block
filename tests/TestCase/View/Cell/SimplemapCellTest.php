<?php
namespace Block\Test\TestCase\View\Cell;

use Block\View\Cell\SimplemapCell;
use Cake\TestSuite\TestCase;

/**
 * Block\View\Cell\SimplemapCell Test Case
 */
class SimplemapCellTest extends TestCase
{

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->request = $this->getMock('Cake\Network\Request');
        $this->response = $this->getMock('Cake\Network\Response');
        $this->Simplemap = new SimplemapCell($this->request, $this->response);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Simplemap);

        parent::tearDown();
    }

    /**
     * Test display method
     *
     * @return void
     */
    public function testDisplay()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
