<?php
namespace Block\Test\TestCase\View\Cell;

use Block\View\Cell\GalleryCell;
use Cake\TestSuite\TestCase;

/**
 * Block\View\Cell\GalleryCell Test Case
 */
class GalleryCellTest extends TestCase
{

  /**
   * setUp method
   *
   * @return void
   */
  public function setUp()
  {
      parent::setUp();
      $this->request = $this->getMock('Cake\Network\Request');
      $this->response = $this->getMock('Cake\Network\Response');
      $this->Gallery = new GalleryCell($this->request, $this->response);
  }

  /**
   * tearDown method
   *
   * @return void
   */
  public function tearDown()
  {
      unset($this->Gallery);

      parent::tearDown();
  }

  public function testEmpty()
  {

  }
}
