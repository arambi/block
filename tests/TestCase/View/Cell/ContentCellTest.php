<?php
namespace Block\Test\TestCase\View\Cell;

use Block\View\Cell\ContentCell;
use Cake\TestSuite\TestCase;

/**
 * Block\View\Cell\ContentCell Test Case
 */
class ContentCellTest extends TestCase
{

  /**
   * setUp method
   *
   * @return void
   */
  public function setUp()
  {
      parent::setUp();
      $this->request = $this->getMock('Cake\Network\Request');
      $this->response = $this->getMock('Cake\Network\Response');
      $this->Content = new ContentCell($this->request, $this->response);
  }

  /**
   * tearDown method
   *
   * @return void
   */
  public function tearDown()
  {
      unset($this->Content);

      parent::tearDown();
  }

  public function testEmpty()
  {

  }
}
