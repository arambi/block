<?php
namespace Block\Test\TestCase\View\Cell;

use Block\View\Cell\BlockCell;
use Cake\TestSuite\TestCase;

/**
 * Block\View\Cell\BlockCell Test Case
 */
class BlockCellTest extends TestCase
{

  /**
   * setUp method
   *
   * @return void
   */
  public function setUp()
  {
      parent::setUp();
      $this->request = $this->getMock('Cake\Network\Request');
      $this->response = $this->getMock('Cake\Network\Response');
      // $this->Block = new BlockCell($this->request, $this->response);
  }

  /**
   * tearDown method
   *
   * @return void
   */
  public function tearDown()
  {
      unset($this->Block);

      parent::tearDown();
  }

  public function testEmpty()
  {

  }

}
