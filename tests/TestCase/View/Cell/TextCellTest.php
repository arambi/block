<?php
namespace Block\Test\TestCase\View\Cell;

use Block\View\Cell\TextCell;
use Cake\TestSuite\TestCase;

/**
 * Block\View\Cell\TextCell Test Case
 */
class TextCellTest extends TestCase
{

  /**
   * setUp method
   *
   * @return void
   */
  public function setUp()
  {
      parent::setUp();
      $this->request = $this->getMock('Cake\Network\Request');
      $this->response = $this->getMock('Cake\Network\Response');
      $this->Text = new TextCell($this->request, $this->response);
  }

  /**
   * tearDown method
   *
   * @return void
   */
  public function tearDown()
  {
      unset($this->Text);

      parent::tearDown();
  }

  public function testEmpty()
  {

  }
}
