<?php
namespace Block\Shell;

use Cake\Console\Shell;
use Cake\Core\Configure;
use Cake\Core\Plugin;
use Cake\Core\App;
use Cake\Utility\Inflector;
use Cake\Filesystem\Folder;

/**
 * Block shell command.
 */
class BlockShell extends Shell
{
  public $tasks = [
    'Bake.BakeTemplate',
  ];

/**
 * Crea una configuración de block
 *
 * @example  bin/cake Block.block create
 * @return void
 */
  public function create()
  {
    $this->__setPathTemplates();
    $default_plugin = $this->getPlugin();
    $plugin = $this->in( 'Indica el plugin donde irá el bloque', [], $default_plugin);
    $plugin = Inflector::camelize( $plugin);

    if( !Plugin::loaded( $plugin))
    {
      $this->error( vsprintf( "El plugin %s no está disponible en la aplicación", [$plugin]));
    }

    $name = $this->in( 'Indica un nombre para el bloque (p.e. Listado de noticias)');
    $key = $this->in( 'Indica un nombre computerizado (p.e. articles)');
    $className = Inflector::camelize( $key);


    $this->dispatchShell( 'bake', 'cell', $plugin .'.'. $key);
    $this->__writeBootstrap( $plugin, $name, $key, $className);
    $this->__writeModel( $plugin, $name, $key, $className);
    $this->__writeCrudView( $plugin, $name, $key, $className);
  }

  public function copy()
  {
    $project = $this->in( 'Indica el nombre del proyecto');
    $parts = explode( DS, ROOT);
    $dir = str_replace( end( $parts), $project, ROOT);
    
    if( !is_dir( $dir))
    {
      $this->error( "No existe el directorio $dir");
    }

    $plugin = ucfirst( $project);
    $current = ucfirst( end( $parts));

    // Cell
    $cellpath = $dir . $this->getPath( 'cell', $plugin);

    $folder = new Folder( $cellpath);
    $list = $folder->read();
    
    $files = $list [1];

    $files = array_map( function( $value){
      return str_replace( 'Cell.php', '', $value);
    }, $files);

    foreach( $files as $key => $file)
    {
      $this->out( $key .'. '. $file );
    }

    $key = $this->in( 'Selecciona un block');

    if( !array_key_exists( $key, $files))
    {
      $this->error( 'El número indicado no existe');
    }
    
    $blockname = $files [$key];
    
    // Cell
    $filepath = $cellpath .DS. $blockname. 'Cell.php'; 

    if( !file_exists( $filepath))
    {
      $this->error( "El fichero $filepath no existe");
    }

    $content = file_get_contents( $filepath);
    $content = str_replace( $plugin, $current, $content);
    $this->createFile( ROOT . $this->getPath( 'cell', $current) .DS. $blockname .'Cell.php', $content);

    // Block
    $blockpath = $dir . $this->getPath( 'block', $plugin);
    $filepath = $blockpath .DS. $blockname .'Block.php';
    $content = file_get_contents( $filepath);
    $content = str_replace( $plugin, $current, $content);
    $this->createFile( ROOT . $this->getPath( 'block', $current) .DS. $blockname .'Block.php', $content);

    // Templates
    $tpath = $dir . $this->getPath( 'template', $plugin) .DS. $blockname;
    $dpath = ROOT . $this->getPath( 'template', $current) .DS. $blockname;
    $folder->copy([
      'from' => $tpath,
      'to' => $dpath
    ]);

    $name = $this->in( 'Indica un nombre');
    $this->__setPathTemplates();
    $this->__writeBootstrap( $current, $name, Inflector::underscore( $blockname), $blockname);
  }

  private function getPath( $type, $plugin)
  {
    $paths = [
      'cell' => DS. 'plugins' .DS. $plugin .DS. 'src' .DS. 'View' .DS. 'Cell',
      'block' => DS. 'plugins' .DS. $plugin .DS. 'src' .DS. 'Model' .DS. 'Block',
      'template' => DS. 'plugins' .DS. $plugin .DS. 'src' .DS. 'Template' .DS. 'Cell'
    ];

    return $paths [$type];
  }

/**
 * Genera el template del block en el plugin indicado
 * 
 * @param  string $plugin
 * @param  string $name
 * @param  string $key
 * @param  string $className
 * @return void
 */
  private function __writeCrudView( $plugin, $name, $key, $className)
  {
    $this->BakeTemplate->set([
      'key' => $key,
      'plugin' => $plugin,
      'className' => $className,
      'name' => $name
    ]);

    $contents = $this->BakeTemplate->generate( 'Shells/view_block');

    $filepath = Plugin::path( $plugin) .'src' .DS. 'Template' .DS. 'Element' .DS. 'crud' .DS. 'blocks' .DS. $key .'.ctp';
    $this->createFile( $filepath, $contents);
  }

/**
 * Genera el fichero Block en el directorio Plugin/Model/ClassNameBlock.php
 * 
 * @param  string $plugin    
 * @param  string $name      
 * @param  string $key       
 * @param  string $className 
 * @return void
 */
  private function __writeModel( $plugin, $name, $key, $className)
  {
    $this->BakeTemplate->set([
      'key' => $key,
      'plugin' => $plugin,
      'className' => $className,
      'name' => $name
    ]);

    $contents = $this->BakeTemplate->generate( 'Shells/model_block');
    $filepath = Plugin::path( $plugin) .'src' .DS. 'Model' .DS. 'Block' .DS. $className .'Block.php';
    $this->createFile( $filepath, $contents);
  }

/**
 * Modifica el fichero bootstrap.php del plugin, añadiendo el BlockRegistry
 * 
 * @param  string $plugin    
 * @param  string $name      
 * @param  string $key       
 * @param  string $className 
 * @return void
 */
  private function __writeBootstrap( $plugin, $name, $key, $className)
  {
    $this->BakeTemplate->set([
      'key' => $key,
      'plugin' => $plugin,
      'className' => $className,
      'name' => $name
    ]);

    $contents = $this->BakeTemplate->generate( 'Shells/registry');

    $bootstrapPath = Plugin::path( $plugin) . 'config' .DS. 'bootstrap.php';
    $bootstrap = file_get_contents( $bootstrapPath);

    $bootstrap .= "\n\n\n\n" . $contents;
    file_put_contents( $bootstrapPath, $bootstrap);
  }

/**
 * Añade el path del plugin para los templates
 * Necesario para que BakeTemplate pueda leer los template
 * 
 * @return void
 */
  private function __setPathTemplates()
  {
    $paths = Configure::read('App.paths.templates');
    $paths [] = Plugin::path( 'Block') . 'src'. DS. 'Template' .DS;
    Configure::write('App.paths.templates', $paths);
  }

  private function getPlugin()
  {
    $dir = (new Folder( ROOT .DS. 'plugins'))->read();
    return $dir [0][0];
  }
}

