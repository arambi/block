<?php

namespace Block\Lib;

use Cake\Core\Configure;

/**
 * Registrador de bloques para añadir y leer
 */

class BlocksRegistry
{
/**
 * Variable donde se guardarán los bloques
 * 
 * @var array
 */
  protected static $_blocks = [];

/**
 * Bloques disponibles para cada model
 * 
 * @var array
 */
  protected static $_availables = [];

/**
 * Bloques NO disponibles para cada model
 * 
 * @var array
 */
  protected static $_notAvailables = [];

/**
 * Añade un conjunto de bloques
 * 
 * @param array $blocks
 */
  public static function add( $key, $data)
  {
    $data ['key'] = $key;
    static::$_blocks [$key] = $data;
  }

/**
 * Devuelve un bloque o un valor de un key de un bloque (si se da)
 * 
 * @param  string $name El nombre computerizado del bloque
 * @param  string $key  El key del bloque
 * @return array|string
 */
  public static function get( $name = null, $key = null)
  {
    if( empty( self::$_blocks))
    {
      self::$_blocks = Configure::read( 'Block');
    }

    if( !$name)
    {
      return self::$_blocks;
    }

    if( !array_key_exists( $name, self::$_blocks))
    {
      // throw new \RuntimeException( "Unable to find '$name' block");
      return;
    }

    $block = self::$_blocks [$name];

    if( $key)
    {
      if( !array_key_exists( $key, $block))
      {
        return false;
      }

      return $block [$key];
    }

    return $block;
  }

/**
 * Devuelve la configuración de varios bloques, dadas un array con sus keys
 * 
 * @param  array $names
 * @return array
 */
  public static function getMany( $names, $model)
  {
    $return = [];
    $notAvailables = [];
    
    if( $model != 'Wrappers')
    {
      $notAvailables [] = 'content';
    }

    if( array_key_exists( $model, self::$_notAvailables))
    {
      $notAvailables = array_merge( $notAvailables, self::$_notAvailables [$model]);
    }


    if( array_key_exists( 'default', self::$_notAvailables))
    {
      $notAvailables = array_merge( $notAvailables, self::$_notAvailables ['default']);
    }

    if( array_key_exists( $model, self::$_availables))
    {
      $availables = self::$_availables [$model];
    }
    else
    {
      $availables = false;
    }

    foreach( $names as $name)
    {
      if( !in_array( $name, $notAvailables) || ( (!$availables && !in_array( $name, $notAvailables)) || in_array( $name, (array)$availables)))
      {
        if( !self::get( $name, 'disableByDefault') || in_array( $name, (array)$availables))
        {
          $return [$name] = self::get( $name);
        }
      }
    }

    $return = collection( $return)->sortBy( 'title', \SORT_ASC, \SORT_NATURAL)->toArray();
    return array_values( $return);
  }

/**
 * Devuelve las configuraciones de los bloques disponibles para un model dado
 * 
 * @param  string $model El alias del model
 * @return array
 */
  public static function getAvailables( $model)
  {
    if( !array_key_exists( $model, self::$_availables))
    {
      return self::get();
    }

    $blocks = self::$_availables [$model];

    $return = [];

    foreach( $blocks as $block)
    {
      $return [$block] = self::get( $block);
    }

    return $return;
  }

/**
 * Configura la variable $_availables
 * 
 * @param  array $config
 * @return void
 */
  public static function availables( $config)
  {
    self::$_availables = array_merge( self::$_availables, $config);
  }

/**
 * Configura la variable $_availables
 * 
 * @param  array $config
 * @return void
 */
  public static function setNotAvailables( $config)
  {
    self::$_notAvailables = array_merge( self::$_notAvailables, $config);
  }

}