
// Bloque <%= $name %>
BlocksRegistry::add( '<%= $key %>', [
    'key' => '<%= $key %>',
    'title' => __d( 'admin', '<%= $name %>'),
    'icon' => 'fa fa-square-o',
    'afterAddTarget' => 'parent',
    'inline' => false,
    'unique' => false,
    'deletable' => true,   
    'className' => '<%= $plugin %>\\Model\\Block\\<%= $className %>Block',
    'cell' => '<%= $plugin %>.<%= $className %>::display',
    'blockView' => '<%= $plugin %>/blocks/<%= $key %>'
]);
