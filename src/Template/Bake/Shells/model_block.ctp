<?php

namespace <%= $plugin %>\Model\Block;

class <%= $className %>Block
{
  public function parse( $Table)
  {
    $Table->crud
      ->setName([
          'singular' => __d( 'admin', '<%= $name %>'),
          'plural' => __d( 'admin', '<%= $name %>'),
        ])
      ->addFields([
        'title' => __d( 'admin', 'Título'),
        'key' => [
          'type' => 'hidden'
        ]
      ]);

    $Table->crud->addView( 'create', [
        'saveButton' => true,
        'columns' => [
          [
            'title' => 'General',
            'key' => 'general',
            'cols' => 12,
            'box' => [
              [
                'title' => null,
                'elements' => [
                  'title',
                ]
              ]
            ],  
          ]
        ]
      ], ['update']);
  }
}