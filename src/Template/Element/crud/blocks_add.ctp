
  <div class="row wrapper border-bottom white-bg page-heading">
    <ng-include src="'header.html'"></ng-include>
  </div>
  <div class="animated fadeIn">
    <div class="row">
        <ng-include src="'Manager/_warning.html'"></ng-include>
        <span ng-click="addBlock( action)" class="btn btn-w-m btn-white btn-blocks" ng-repeat="(key, action) in data.actions" class="col-xs-3 padder-v text-muted text-center">
          <i class="{{ action.icon }} block m-b-xs fa-2x"></i>
          <span>{{ action.title }}</span>
        </span>
    </div>
  </div>
