<div class="form-group">
  <label ng-help tooltip="{{ field.help }}" class="col-sm-2 control-label">{{ field.label }}</label>
  <div class="col-sm-10">
    <div class="row">
      <div class="col-sm-12">
        <button class="btn btn-success pl-upload-button" 
            pl-upload pl-files-model="content.settings.background_image" 
            pl-url="/upload/Uploads/upload.json?key={{ field.config.type }}" 
            ng-show="!plUploading">
            <i class="fa fa-arrow-circle-o-up"></i>
        </button>
      </div>
    </div>
    <pl-upload-progress ng-model="isUploading"></pl-upload-progress>
    <div class="cf-thumbail" ng-if="content.settings.background_image.paths[field.config.size]">
      <pl-upload-image pl-type="single" pl-src="content.settings.background_image.paths[field.config.size]" pl-files-model="content.settings.background_image"></pl-upload-image>
    </div>
  </div>
</div>