<div class="row">
  <div class="col-md-7">
    <div class="block-design">
      <div class="block-margin">
        <label>margin</label>
        <input ng-model="content.settings.margin_top" class="block-top" type="text">
        <input ng-model="content.settings.margin_right" class="block-right" type="text">
        <input ng-model="content.settings.margin_bottom" class="block-bottom" type="text">
        <input ng-model="content.settings.margin_left" class="block-left" type="text">
        <div class="block-border">
          <label>border</label>
          <input ng-model="content.settings.border_top_width" class="block-top" type="text">
          <input ng-model="content.settings.border_right_width" class="block-right" type="text">
          <input ng-model="content.settings.border_bottom_width" class="block-bottom"  type="text">
          <input nng-model="content.settings.border_left_width" class="block-left"  type="text">
          <div class="block-padding">
            <label>padding</label>
            <input ng-model="content.settings.padding_top" class="block-top" type="text">
            <input ng-model="content.settings.padding_right" class="block-right" type="text">
            <input ng-model="content.settings.padding_bottom" class="block-bottom" type="text">
            <input ng-model="content.settings.padding_left" class="block-left" type="text">
            <div class="block-content"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-5 form-vertical">
      <crud-field ng-init="content = content" field="settings.with_border"></crud-field>
      <crud-field ng-init="content = content" field="settings.border_color"></crud-field>
      <crud-field ng-init="content = content" field="settings.border_style"></crud-field>
      <crud-field ng-init="content = content" field="settings.border_radius"></crud-field>
      <crud-field ng-init="content = content" field="settings.background_color"></crud-field>
      <crud-field ng-init="content = content" field="settings.background_image"></crud-field>
      <crud-field ng-if="content.settings.background_image" ng-init="content = content" field="settings.background_size"></crud-field>
      <crud-field ng-if="content.settings.background_image" ng-init="content = content" field="settings.background_position_horizontal"></crud-field>
      <crud-field ng-if="content.settings.background_image" ng-init="content = content" field="settings.background_position_vertical"></crud-field>
  </div>
</div>