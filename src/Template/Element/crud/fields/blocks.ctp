<h4 class="label-block">{{ field.label }}</h4>
<div class="cf-row-add"><a cf-add-row index="0" tooltip="<?= __d( 'admin', 'Añadir fila') ?>"><i class="fa fa-plus"></i></a></div>
<div ui-sortable="sortableRow" ng-model="content.rows" class="" >
  <!-- Row -->
  <div ng-repeat="row in content.rows" class="row row-container" cf-columns ng-model="row.columns">
    <div class="col-lg-12">
      <div class="cf-row-wrapper">
        <input type="hidden" ng-model="row.position" ng-bind="row.position = $index + 1" />
        <span tooltip="<?= __d( 'admin', 'Mover fila') ?>" class="handle handle-row row-button"><i class="fa fa-arrows"></i></span>
        <span class="btn row-button row-edit" cf-modal-update="'/admin/block/rows/serialize/update'" cf-after-action="update" ng-model="row"><i class="fa fa-edit"></i></span>
        <?php if( \Cake\Core\Configure::read('Block.paddings')): ?>
          <span class="row-button">
            <label><i class="fa fa-long-arrow-up"></i></label>
            <select class="form-control" style="width: 30px; min-width: 0; padding: 0" ng-model="row.settings.pt">
              <?php for( $i = 0; $i <= \Cake\Core\Configure::read('Block.paddings'); $i++): ?>
                <option value="<?= $i ?>"><?= $i ?></option>
              <?php endfor ?>
            </select>
          </span>
          <span class="row-button">
            <label for=""><i class="fa fa-long-arrow-down"></i></label>
            <select class="form-control" style="width: 30px; min-width: 0; padding: 0" ng-model="row.settings.pb">
              <?php for( $i = 0; $i <= \Cake\Core\Configure::read('Block.paddings'); $i++): ?>
                <option value="<?= $i ?>"><?= $i ?></option>
              <?php endfor ?>
            </select>
          </span>
        <?php endif ?>

        <?php if( !\Cake\Core\Configure::read('Block.noColumns')): ?>
          <span class="row-button columns-layouts"><span class="layout-current layout-1"></span>
            <span class="layout-1" cf-columns-layout="1/1" ng-model="row"></span>
            <span class="layout-12_12" cf-columns-layout="1/2 + 1/2" ng-model="row"></span>
            <span class="layout-13_13_13" cf-columns-layout="1/3 + 1/3 + 1/3" ng-model="row"></span>
            <span class="layout-14_12_14" cf-columns-layout="1/4 + 1/2 + 1/4" ng-model="row"></span>
            <span class="layout-14_14_14_14" cf-columns-layout="1/4 + 1/4 + 1/4 + 1/4" ng-model="row"></span>
            <span class="layout-16_46_16" cf-columns-layout="1/6 + 4/6 + 1/6" ng-model="row"></span>
            <span class="layout-16_16_16_12" cf-columns-layout="1/6 + 1/6 + 1/6 + 1/2" ng-model="row"></span>
            <span class="layout-16_16_16_16_16_16" cf-columns-layout="1/6 + 1/6 + 1/6 + 1/6 + 1/6 + 1/6" ng-model="row"></span>
            <span class="layout-23_13" cf-columns-layout="2/3 + 1/3" ng-model="row"></span>
            <span class="layout-56_16" cf-columns-layout="5/6 + 1/6" ng-model="row"></span>
          </span>
        <?php endif ?>
        
        <span class="btn btn-danger" ng-if="!row.published"><i class="fa fa-remove"></i> <?= __d( 'app', 'No publicado') ?></span>
        
        <div class="row">
          
          <!-- Columns -->
          <div ui-sortable="sortableColumn" ng-model="row.columns">
            <div ng-repeat="column in row.columns" cf-blocks ng-model="column.blocks" class="column-container col-lg-{{ column.cols }}">
              <div class="column-wrapper">
                <div class="column-buttons">
                  <span tooltip="<?= __d( 'admin', 'Mover colunma') ?>" class="handle handle-column"><i class="fa fa-arrows"></i></span>
                  <span class="btn" cf-modal-update="'/admin/block/columns/serialize/update'" cf-after-action="update" ng-model="column"><i class="fa fa-pencil"></i></span>
                  <span class="btn" tooltip="<?= __d( 'admin', 'Borrar columna') ?>" 
                              confirm-delete 
                              cf-destroy="scope.row.columns.splice( scope.$index, 1);"><i class="warning fa fa-trash"></i></span>
                  <span ng-if="!column.published" class="btn  btn-danger"><i class="fa fa-remove"></i> <?= __d( 'app', 'No publicado') ?></span>
                </div>
                  
                <div class="cf-block-add">
                    <a  tooltip="<?= __d( 'admin', 'Añadir bloque') ?>" 
                        cf-add-block 
                        row-index="{{ $parent.$index }}" 
                        column-index="{{ $index }}" 
                        block-index="0"><i class="fa fa-plus"></i></a>
                </div>

                <div ui-sortable="sortableBlock" ng-model="column.blocks" class="column-zone column-container-{{ crudConfig.blockWrapper }}">

                  <!-- Block -->
                  <div ng-repeat="block in column.blocks" ng-model="column.blocks" class="block-container">
                    <input type="hidden" ng-model="block.position" ng-bind="column.block = $index + 1" />
                    <div class="ibox float-e-margins cf-block">
                      <!-- Botones de ediciÃ³n del bloque -->
                      <div class="ibox-title">
                        <h5>{{ block.block_type.title }}</h5>
                        <div class="ibox-tools dropdown">
                          <span class="handle handle-block" href tooltip="<?= __d( 'admin', 'Mover bloque') ?>" ><i class="fa fa-arrows"></i></span>
                          <a tooltip="<?= __d( 'admin', 'Editar bloque') ?>" href cf-edit-block> <i class="fa fa-pencil"></i></a>
                          <span class="pointer" ng-if="block.block_type.deletable" tooltip="<?= __d( 'admin', 'Borrar bloque') ?>" 
                              confirm-delete 
                              cf-destroy="scope.column.blocks.splice( scope.$index, 1);"><i class="warning fa fa-trash"></i></span>
                          <span ng-if="!block.published" style="color: #fff" class="btn btn-danger"><i class="fa fa-remove"></i> <?= __d( 'app', 'No publicado') ?></span>
                        </div>
                      </div>
                      <!-- /Botones de ediciÃ³n del bloque -->

                      <!-- Contenido del bloque -->
                      <div class="ibox-content">
                        <input type="hidden" ng-model="block.cols" />
                        <ng-include src="block.block_type.blockView + '.html'" ng-init="block = block"></ng-include>
                      </div>
                      <!-- /Contenido del bloque -->
                    </div>
                    
                    <div class="cf-block-add">
                        <a  tooltip="<?= __d( 'admin', 'Añadir bloque') ?>" 
                            cf-add-block 
                            row-index="{{ $parent.$parent.$index }}" 
                            column-index="{{ $parent.$index }}" 
                            block-index="{{ $index + 1 }}"><i class="fa fa-plus"></i></a>
                    </div>

                  </div>
                  <!-- /Block -->

                </div>
              </div>
            </div>
          </div>
          <!-- /Columns -->
            
        </div>
      </div>
      <div class="cf-row-add"><a tooltip="<?= __d( 'admin', 'Añadir fila') ?>" cf-add-row index="{{ $index + 1}}" ><i class="fa fa-plus"></i></a></div>
      
    </div>
  </div>
  <!-- /Rows -->
</div>