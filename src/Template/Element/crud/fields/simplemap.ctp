
<div cf-google-events="{ lat: field.options.fields.lat, lng: field.options.fields.lng, zoom: field.options.fields.zoom }">
  <input places-auto-complete types="['geocode']" class="form-control"  on-place-changed="gmap.simpleMapLocation()" />
  <map center="{{ content.settings.lat }}, {{ content.settings.lng }}" zoom="15" on-zoom-changed="gmap.zoomChanged()">
    <marker position="{{ content.settings.lat }}, {{ content.settings.lng }}" draggable="true" on-dragend="gmap.dragend()"></marker>
  </map>
</div>
{{ content.settings.lat }}, {{ content.settings.lng }} - {{ content.settings.zoom }}
