<div class="cf-form">
  <button cf-add-has-many="crudConfig.view.fields.tabs.crudConfigAdd" ng-model="content.tabs" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> <?= __d( 'admin', 'Añadir pestaña') ?></button>
  
  <div tabset class="m-t-2">
    <div tab ng-repeat="tab in content.tabs" ng-model="content.tabs" >
      <div tab-heading>
        {{ tab[data.currentLanguage.iso3].title }}
      </div>
      <div class="panel-body">
        <crud-field ng-init="crudConfig = crudConfig.view.fields.tabs.crudConfigAdd; content = tab" field="title"></crud-field>
        <crud-field ng-init="crudConfig = crudConfig.view.fields.tabs.crudConfigAdd; content = tab" field="body"></crud-field>


        <span class="btn btn-danger btn-sm" tooltip="<?= __d( 'admin', 'Borrar') ?>" confirm-delete cf-destroy="scope.content.tabs.splice( scope.$index, 1);"><i class="warning fa fa-trash"></i> <?= __d( 'admin', 'Borrar pestaña') ?></span>
      </div>
    </div>
  </div>
</div>