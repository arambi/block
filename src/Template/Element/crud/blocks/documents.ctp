<div class="row">
  <div class="col-sm-12">
    <div class="block-documents">
      <h2>{{ block.title }}</h2>
      <div ng-repeat="doc in block.docs">{{ doc.filename }}</div>
    </div>
  </div>
</div>