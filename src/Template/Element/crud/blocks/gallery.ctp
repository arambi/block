<div class="row">
  <div class="col-sm-12">
    <div class="block-gallery" ng-repeat="photo in block.photo">
      <img style="width: 100%" ng-if="photo.id" ng-src="{{ photo.paths.thm }}?{{ photo.modified | date:'yyyyMMddHHmmss' }}" />
    </div>
  </div>
</div>