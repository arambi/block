<div <?= !empty( $block->settings->align) ? ' style="text-align:' . $block->settings->align .'"' : ''  ?>>
  <?php if( !empty( $block->url)): ?>
      <a <?= !empty( @$block->settings->target_blank) ? 'target="_blank"' : '' ?> href="<?= $block->url ?>">
  <?php endif ?>
  <?= $this->Uploads->image( $block->photo, 'org') ?>
  <?php if( !empty( $block->url)): ?>
      </a>
  <?php endif ?>
</div>
