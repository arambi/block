<h3><?= $block->title ?></h3>

<div class="block-doc-docs b-documents__docs">
  <?php foreach( $block->docs->locales() as $doc): ?>
      <a download href="<?= $doc->paths[0] ?>" class="block-doc-ext-<?= $doc->extension ?>">
        <span class="block-doc-icon b-documents__icon"><i class="<?= $doc->icon ?>"></i></span>
        <div class="block-doc-content b-documents__content">
          <span class="block-doc-title b-documents__title"><?= $doc->title() ?></span>
          <span class="block-doc-size b-documents__size"><?= $doc->filesize() ?></span>
        </div>
      </a>
  <?php endforeach ?>
</div>
  