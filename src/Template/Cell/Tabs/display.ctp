<div class="row">
  <div class="col-sm-4 col-md-3">
    <ul class="nav nav-tabs vertical">
    <?php foreach( $tabs as $key => $tab): ?>
      <li class="<?= $key == 0 ? 'active' : '' ?>" ><a href="#tab-<?= $tab->id ?>" data-toggle="tab"><?= $tab->title ?></a></li>
    <?php endforeach ?>
    </ul>
  </div>
  <div class="col-sm-8 col-md-9">
    <div class="tab-content vertical">
    <?php foreach( $tabs as $key => $tab): ?>
      <div class="tab-pane fade in <?= $key == 0 ? 'active' : '' ?>" id="tab-<?= $tab->id ?>">
        <?= $tab->body ?>
      </div>
    <?php endforeach ?>
    </div>
  </div>
</div>