<?php if( !empty( $block->title)): ?>
  <h3><?= $block->title ?></h3>
<?php endif ?>

<div class="b-links__contents">
  <?php foreach( $contents as $content): ?>
    <a <?= !empty( @$content->target_blank) ? 'target="_blank"' : '' ?> class="b-links__contents__link b-links__contents__link--<?= $content->type ?>" href="<?= $content->link ?>"><?= $content->title ?></a>
  <?php endforeach ?>
</div>