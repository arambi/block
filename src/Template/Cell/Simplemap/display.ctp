<div class="simplemap" id="simplemap-<?= $block->id ?>" style="height: <?= $block->settings->height ?>px"></div>
<?php $this->Buffer->start( 'initMap') ?>
  var center = {lat: <?= str_replace( ',', '.', $block->settings->lat) ?>, lng: <?= str_replace( ',', '.', $block->settings->lng) ?>};
  var map = new google.maps.Map(document.getElementById( 'simplemap-<?= $block->id ?>'), {
    zoom: <?= $block->settings->zoom ?>,
    center: center
    <?php if( !empty( $block->settings->styles)): ?>
        , styles: <?= $block->settings->styles ?>
    <?php endif ?>
  });

  var marker = new google.maps.Marker({
    position: {lat: <?= str_replace( ',', '.', $block->settings->lat) ?>, lng: <?= str_replace( ',', '.', $block->settings->lng) ?>},
    map: map
    <?php if( !empty( $block->settings->icon)): ?>
      ,icon: "<?= $block->settings->icon ?>"
    <?php endif ?>
  });

  <?php $this->Buffer->end() ?>
</script>

<?php if( !\Cake\Core\Configure::read( 'GooglemapsInit')): ?>
<?php $this->Buffer->start() ?>
  <script src="//maps.googleapis.com/maps/api/js?callback=initMap&key=<?= \Website\Lib\Website::get( 'settings.google_maps') ?>" async defer></script>
<?php $this->Buffer->end() ?>
  <?php \Cake\Core\Configure::write( 'GooglemapsInit', true) ?>
<?php endif ?>
