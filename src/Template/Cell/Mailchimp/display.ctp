<form name="mc-embedded-subscribe-form" id="mc-embedded-subscribe-form" action="<?= $block->settings->action ?>" method="post" target="_blank" class="validate newsletter-validate-detail" novalidate>
  <div class="subscribe-content">
    <div class="actions">
    </div>
    <!-- .actions -->
    <div class="input-box mc-field-group">
      <input type="text" name="EMAIL" id="mce-EMAIL" title="<?= $block->title ?>" class="input-text required email" placeholder="<?= $block->title ?>" />
      <button type="submit" title="Subscribe" name="subscribe" class="button">
        <span><?= __d( 'app', 'Enviar') ?></span>
      </button>
    </div>
    <div id="mce-responses" class="mce-responses">
      <div class="response" id="mce-error-response" style="display:none"></div>
      <div class="response" id="mce-success-response" style="display:none"></div>
    </div>
  </div>
  <!-- .suscribe-content -->
  <div aria-hidden="true">
    <input type="hidden" name="<?= $block->settings->inputcode ?>" tabindex="-1" value="">
  </div>
</form>


<?php $this->loadHelper( 'Cofree.Buffer')->start() ?>
  <?= $script_mailchimp ?>
<?php $this->Buffer->end() ?>