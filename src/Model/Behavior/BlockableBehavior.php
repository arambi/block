<?php

namespace Block\Model\Behavior;

use ArrayObject;
use I18n\Lib\Lang;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\ORM\Entity;
use Cake\Event\Event;
use Cake\ORM\Behavior;
use Cake\Core\Configure;
use Cake\Event\EventManager;
use Block\Lib\BlocksRegistry;
use Block\Model\Entity\Block;
use Cake\ORM\TableRegistry;

/**
 * Blockable behavior
 */
class BlockableBehavior extends Behavior
{
    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    /**
     * Initialize hook
     *
     * @param array $config The config for this behavior.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->_table->hasMany('Rows', [
            'foreignKey' => 'content_id',
            'className' => 'Block.Rows',
            'sort' => ['Rows.position' => 'asc'],
            'conditions' => ['Rows.model' => $this->_table->getAlias()],
            'dependent' => true
        ]);

        $this->_table->crud->addAssociations(['Blocks', 'Rows', 'Columns']);

        // CrudConfig
        // Añade la configuración de los bloques al crud, para que se pueda setear en la vista
        $blocks = BlocksRegistry::getAvailables($this->_table->getAlias());
        $this->_table->crud->addConfig(['blocks' => $blocks]);

        $this->_table->crud->addConfig(['blockWrapper' => $this->_table->getAlias()]);

        // Añade los fields
        $this->_table->crud->addFields([
            'blocks' => [
                'label' => __d('admin', 'Contenido'),
                'type' => 'block',
                'adapter' => 'block',
                'template' => 'Block.fields.blocks'
            ],
        ]);

        $this->_table->crud->unsetDefaultFunctions();

        // Añade los bloques por defecto
        if (!empty($config['defaults'])) {
            $defaults = $config['defaults'];
            $this->_table->crud->defaults(function ($data) use ($defaults) {
                $index = 0;
                $data['rows'] = [];
                $BlocksTable = TableRegistry::getTableLocator()->get('Block.Blocks');
                
                foreach ($defaults as $key => $name) {
                    if (is_array($name)) {
                        $defaultOptions = $name;
                        $name = $key;
                    } else {
                        $defaultOptions = [];
                    }
                    
                    $block = BlocksRegistry::get($name);
                    $class = $block['className'];
                    $Block = new $class();
                    $defaults = array_merge([
                        'subtype' => $name,
                        'position' => 1,
                    ], $defaultOptions);

                    if (method_exists($Block, 'defaults')) {
                        $defaults = array_merge($defaults, $Block->defaults());
                    }

                    $newblock = $defaults;
                    $newblock['block_type'] = $block;
                    $newblock['published'] = true;

                    $entity = $BlocksTable->save($BlocksTable->newEntity($newblock));

                    $data['rows'][] = [
                        'position' => 1,
                        'published' => true,
                        'columns' => [
                            [
                                'published' => true,
                                'blocks' => [
                                    0 => $entity->toArray()
                                ],
                                'position' => 1,
                                'cols' => 12,
                            ]
                        ]
                    ];

                    $index++;
                }
                return $data;
            });
        } else {
            $this->_table->crud->defaults();
        }
    }

    /**
     * Añade el contain para los bloques
     * 
     * @param  Event  $event   
     * @param  Query  $query   
     * @param  array $options 
     */
    public function beforeFind(Event $event, Query $query, ArrayObject $options)
    {
        if (!$this->_table->isAdmin()) {
            $query->contain([
                'Rows' => function ($q) {
                    return $q
                        ->find('published')
                        ->contain([
                            'Columns' => function ($q) {
                                return $q
                                    ->find('published')
                                    ->contain([
                                        'Blocks' => function ($q) {
                                            $event = new Event('Block.Model.Table.Blocks.findFront', $this, [
                                                $q
                                            ]);

                                            EventManager::instance()->dispatch($event);

                                            $q->formatResults(function ($results) {
                                                return $results->filter(function ($row) {
                                                    if (!is_object($row->settings) || !property_exists($row->settings, 'restrict_language') || !$row->settings->restrict_language) {
                                                        return true;
                                                    }

                                                    $languages = $row->settings->languages;
                                                    $current = Lang::current('iso3');
                                                    $langs = explode(',', $languages);
                                                    return in_array($current, $langs);
                                                });
                                            });

                                            return $q->find('published');
                                        }
                                    ]);
                            }
                        ]);
                }
            ]);
        } else {
            if ($this->_table->crud->action() != 'index') {
                $query->contain([
                    'Rows' => [
                        'Columns' => [
                            'Blocks'
                        ]
                    ]
                ]);
            }
        }
    }

    public function beforeSave(Event $event, Entity $entity, ArrayObject $options)
    {
        if ($entity->has('rows') && is_array($entity->rows)) {
            foreach ($entity->rows as $key => $row) {
                if (is_object($row)) {
                    $row->set('model', $this->_table->alias());
                }
            }
        }
    }

    public function afterSave(Event $event, Entity $entity)
    {
        if (empty($entity->rows)) {
            return;
        }

        foreach ($entity->rows as $key => $row) {
            if (is_object($row)) {
                if (empty($row->columns)) {
                    $content = $this->_table->Rows->findById($row->id)->first();
                    $this->_table->Rows->delete($content);
                }
            }
        }
    }
}
