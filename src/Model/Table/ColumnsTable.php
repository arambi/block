<?php

namespace Block\Model\Table;

use ArrayObject;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\ORM\Entity;
use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\ORM\RulesChecker;
use Block\Model\Entity\Column;
use Cake\Validation\Validator;
use Block\Model\Table\BlocksSettingsTrait;


/**
 * Columns Model
 */
class ColumnsTable extends Table
{

    use BlocksSettingsTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('columns');
        $this->displayField('id');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');

        // Behaviors
        $this->addBehavior('Manager.Crudable');
        $this->addBehavior('Cofree.Saltable');
        $this->addBehavior('Cofree.Publisher');

        $this->addBehavior('Cofree.Jsonable', [
            'fields' => ['settings']
        ]);

        $this->hasMany('Blocks', [
            'alias' => 'Blocks',
            'foreignKey' => 'category_id',
            'className' => 'Block.Blocks',
            'sort' => ['Blocks.position'],
            'dependent' => true
        ]);

        // CRUD Config

        $this->crud
            ->setName([
                'singular' => __d('admin', 'Columna'),
                'plural' => __d('admin', 'Columnas'),
            ])
            ->addView('update', [

                'columns' => [],
                'actionButtons' => [],
            ])
            ->addFields([
                'cols' => [
                    'label' => __d('admin', 'Número de columnas'),
                    'help' => __d('admin', 'El número de columnas que ocupa (el ancho completo son 12 columnas)'),
                    'type' => 'select',
                    'options' => [1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12]
                ],
                'settings.responsive' => [
                    'label' => __d('admin', 'Distintas columnas para los dispositivos'),
                    'type' => 'boolean'
                ],
                'settings.xs' => [
                    'label' => __d('admin', 'Móvil'),
                    'help' => __d('admin', 'El número de columnas que ocupa en dispositivo móvil (el ancho completo son 12 columnas)'),
                    'type' => 'select',
                    'options' => [1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12],
                    'show' => 'content.settings.responsive'
                ],
                'settings.sm' => [
                    'label' => __d('admin', 'Tablet'),
                    'help' => __d('admin', 'El número de columnas que ocupa en dispositivo tablet (el ancho completo son 12 columnas)'),
                    'type' => 'select',
                    'options' => [1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12],
                    'show' => 'content.settings.responsive'
                ],
                'settings.md' => [
                    'label' => __d('admin', 'Portátil'),
                    'help' => __d('admin', 'El número de columnas que ocupa en dispositivo portátil (el ancho completo son 12 columnas)'),
                    'type' => 'select',
                    'options' => [1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12],
                    'show' => 'content.settings.responsive'
                ],
            ]);

        // Llamando al método de BlocksSettingsTrait
        $this->_setGeneral();

        if (!Configure::read('Block.noColumns')) {
            $this->crud->addBoxToColumn('update', 'general', [
                'elements' => [
                    'cols',
                    'settings.responsive',
                    'settings.xs',
                    'settings.sm',
                    'settings.md'
                ]
            ]);
        }
    }


    public function beforeSave(Event $event, Entity $entity, ArrayObject $options)
    {
        if (!empty($entity->id) && !empty($entity->blocks)) {
            foreach ($entity->blocks as $block) {
                $block->category_id = $entity->id;
            }
        }
    }
}
