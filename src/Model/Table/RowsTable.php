<?php
namespace Block\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\Entity;
use Cake\Event\Event;
use ArrayObject;
use Block\Model\Table\BlocksSettingsTrait;

/**
 * Rows Model
 */
class RowsTable extends Table 
{

	use BlocksSettingsTrait;

/**
 * Initialize method
 *
 * @param array $config The configuration for the Table.
 * @return void
 */
	public function initialize(array $config) 
	{
		$this->table('rows');
		$this->displayField('id');
		$this->primaryKey('id');

		// Behaviors
		$this->addBehavior('Timestamp');
		$this->addBehavior( 'Cofree.Saltable');
    $this->addBehavior( 'Manager.Crudable');
    $this->addBehavior( 'Cofree.Publisher');

		$this->addBehavior( 'Cofree.Jsonable', [
      'fields' => ['settings']
    ]);
		
		$this->hasMany( 'Columns', [
			'alias' => 'Columns',
			'foreignKey' => 'row_id', 
			'className' => 'Block.Columns',
			'sort' => ['Columns.position'],
			'dependent' => true
		]);

		$this->crud
			->setName( [
				'singular' => __d( 'admin', 'Fila'),
				'plural' => __d( 'admin', 'Filas'),
			])
			->addView( 'update', [
        'columns' => [
          
        ],
        'actionButtons' => []
      ]);

		// Llamando al método de BlocksSettingsTrait
    $this->_setGeneral();
	}



}
