<?php
namespace Block\Model\Table;

use Block\Model\Entity\Tab;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Core\Configure;

/**
 * Tabs Model
 */
class TabsTable extends Table
{

  /**
   * Initialize method
   *
   * @param array $config The configuration for the Table.
   * @return void
   */
  public function initialize(array $config)
  {
    parent::initialize($config);

    $this->table('contents');
    $this->displayField('title');
    $this->primaryKey('id');

    $this->addBehavior('Timestamp');
    
    // Behaviors
    $this->addBehavior( 'Manager.Crudable');
    $this->addBehavior( 'Cofree.Saltable');
    $this->addBehavior( 'Cofree.Contentable');
    $this->addBehavior( Configure::read( 'I18n.behavior'), [
      'fields' => ['title','body']
    ]);

    // CRUD Config
    //Escribe aquí las asociaciones vinculadas con el model a la hora de editar (si las hubiera)
    // $this->crud->associations([]);

    $this->crud
      ->addFields([
        'title' => __d( 'admin', 'Título'),
        'body' => __d( 'admin', 'Cuerpo de texto'),
        
      ])
      ->addIndex( 'index', [
        'fields' => [
          'title',
        
        ],
        'actionButtons' => ['create'],
        'saveButton' => false,
      ])
      ->setName( [
        'singular' => __d( 'admin', 'Pestanas'),
        'plural' => __d( 'admin', 'Pestanas'),
      ])
      ->addView( 'create', [
        'columns' => [
          [
            'cols' => 8,
            'box' => [
              [
                'elements' => [
                  'title',
                  'body',
                ]
              ]
            ]
          ]
        ],
        'actionButtons' => []
      ], ['update'])
      ;
    
  }
}
