<?php
namespace Block\Model\Table;

use ArrayObject;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\Event\EventManager;
use Block\Lib\BlocksRegistry;
use Cake\Validation\Validator;
use Cake\Datasource\EntityInterface;
use Block\Model\Table\BlocksSettingsTrait;

/**
 * Blocks Model
 */
class BlocksTable extends Table 
{
	use BlocksSettingsTrait;
/**
 * Initialize method
 *
 * @param array $config The configuration for the Table.
 * @return void
 */
	public function initialize(array $config) 
	{
		$this->table( 'contents');
		$this->displayField( 'title');
		$this->primaryKey( 'id');
		$this->addBehavior( 'Timestamp');
		$this->addBehavior( Configure::read( 'I18n.behavior'), [
      'fields' => ['title', 'subtitle', 'body', 'video']
		]);
		
		$this->addBehavior( 'Upload.UploadJsonable', [
			'fields' => [
				'photo',
				'photos',
				'docs',
			]
		]);
		$this->addBehavior( 'Cofree.Contentable');
		$this->addBehavior( 'Manager.Crudable');
		$this->addBehavior( 'Cofree.Publisher');
		
		
		$this->addBehavior( 'Cofree.Jsonable', [
			'fields' => [
				'settings'
			]
		]);
		
		$this->crud->addFields([
			'published' => __d( 'admin', 'Publicado')
		]);

		$this->crud->addView( 'add', [
			'template' => 'Block/blocks_add',
			'columns' => [
				[
					'cols' => 12,
					'box' => [
						[
							'title' => null,
							'elements' => [
								
							]
						]
					],
				],
			],
			'saveButton' => false
		])
		->setName( [
				'singular' => __d( 'admin', 'Bloque'),
				'plural' => __d( 'admin', 'Bloques'),
		]);

		$this->crud->defaults([
			'cols' => 12,
			'published' => 1
		]);

		$this->crud->addView( 'create', [
			'columns' => [
				
			],
			'saveButton' => true
		], 'update');


		// Llamando al método de BlocksSettingsTrait
	}

	public function beforeSave( Event $event, EntityInterface $entity, ArrayObject $options)
	{
		if( $entity->has( 'subtype'))
		{
			$object = $this->getBlockObject( $entity->subtype);

			if( method_exists( $object, 'beforeSave'))
			{
				$object->beforeSave( $this, $entity, $options);
			}
		}
			
	}

	public function getBlockObject( $type)
	{
		$blockInfo = BlocksRegistry::get( $type);
		$namespace = $blockInfo ['className'];
		return new $namespace;
	}
/**
 * Acciones de modificación de resultados
 * 	
 * @param  Event $event
 * @param  Query  $query
 * @return void
 */
	public function beforeFind( $event, Query $query)
	{
		$query->formatResults(function ($results) {
			return $this->_rowMapper($results);
		}, $query::PREPEND);
	}

/**
 * Coloca la información del tipo de block (disponible en Configure), 
 * 	para que pueda usar esa información en las vistas
 * 	
 * @param Collection|Entity $results 
 * @return void
 */
	protected function _rowMapper( $results) 
	{
		return $results->map(function ($row) {
			if ($row === null) {
				return $row;
			}

			// Setea la información del bloque
			if( is_object( $row))
			{
				$row->set( 'block_type', BlocksRegistry::get( $row->subtype));

				if( $row->subtype == 'reference')
				{
					$reference = $this->find()
			      ->where([
			        'Blocks.id' => $row->parent_id
			      ])
			      ->first();
					
					if( $reference)
					{
						$row->set( 'reference', $reference);
						$row->set( 'subtype', $reference->subtype);
					}
				}
			}			
				
			return $row;
		});
	}

}
