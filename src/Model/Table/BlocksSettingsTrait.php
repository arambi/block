<?php

namespace Block\Model\Table;

use Cake\Core\Configure;
use I18n\Lib\Lang;

/**
 * Configuración del CRUD para los estilos y propiedades de los bloques, filas y columnas
 */

trait BlocksSettingsTrait
{
    public function _setGeneral($CustomTable = null)
    {
        $table = $CustomTable ? $CustomTable : $this;

        $updateElements = [
            'published' => 'Publicado',
            'settings.id',
            'settings.class',
            'settings.wrapper_div',
            'settings.wrapper_div_id',
            'settings.wrapper_div_class',
        ];

        if ($table->alias() == 'Rows') {
            $updateElements[] = 'settings.fullwidth';
        }

        if (Configure::read('Block.paddings')) {
            
        }

        if (Configure::read('Block.customClasses')) {
            $options = Configure::read('Block.customClasses') + ['none' => __d('admin', '-- Ninguno --')];

            $table->crud->addFields([
                'published' => __d('admin', 'Publicado'),
                'settings.custom_classes' => [
                    'label' => __d('admin', 'Estilos determinados'),
                    'help' => __d('admin', 'Estilos para este elemento'),
                    'type' => 'checkoptions',
                    'options' =>  Configure::read('Block.customClasses')
                ],
            ]);



            $updateElements[] = 'settings.custom_classes';
        }

        if (__CLASS__ == 'Block\Model\Table\BlocksTable') {
            $table->crud->addFields([
                'settings.restrict_language' => [
                    'type' => 'boolean',
                    'options' => Lang::iso3(),
                    'label' => 'Mostrar solo para unos idiomas'
                ],
                'settings.languages' => [
                    'type' => 'checkoptions',
                    'options' => Lang::iso3(),
                    'label' => 'Idiomas',
                    'show' => 'content.settings.restrict_language'
                ]
            ]);

            $updateElements[] = 'settings.restrict_language';
            $updateElements[] = 'settings.languages';
        }

        $table->crud
            ->addFields([
                'settings.fullwidth' => [
                    'label' => __d('admin', 'Ocupar todo el ancho de ventana'),
                    'help' => __d('admin', 'Si seleccionas esta opción el fondo de la fila ocupará todo el ancho de la ventana'),
                    'type' => 'boolean',
                ],
                'settings.wrapper_div' => [
                    'label' => __d('admin', 'Envolver el contenido en un elemento div'),
                    'help' => __d('admin', 'Si seleccionas esta opción todo el bloque o fila quedará dentro de un elemento div'),
                    'type' => 'boolean',
                    'show' => 'administrator.superadmin'
                ],
                'settings.wrapper_div_id' => [
                    'label' => __d('admin', 'Id del elemento envoltorio'),
                    'help' => __d('admin', 'Id único para este elemento'),
                    'type' => 'string',
                    'show' => 'administrator.superadmin && content.settings.wrapper_div',
                ],
                'settings.wrapper_div_class' => [
                    'label' => __d('admin', 'CSS Class del elemento envoltorio'),
                    'help' => __d('admin', 'Clase CSS para el elemento'),
                    'type' => 'string',
                    'show' => 'administrator.superadmin && content.settings.wrapper_div',
                ],
                'settings.id' => [
                    'label' => __d('admin', 'Id'),
                    'help' => __d('admin', 'Id único para este elemento'),
                    'type' => 'string',
                    'show' => 'administrator.superadmin'
                ],
                'settings.class' => [
                    'label' => __d('admin', 'CSS Class'),
                    'help' => __d('admin', 'Clase CSS para el elemento'),
                    'type' => 'string',
                    'show' => 'administrator.superadmin'
                ],
                'settings.design' => [
                    'label' => __d('admin', 'Diseño'),
                    'type' => 'string',
                    'template' => 'Block/fields/block_design'
                ],
                'settings.with_border' => [
                    'label' => __d('admin', 'Con borde'),
                    'help' => __d('admin', 'Selecciona esta opción si quieres que el elemento tenga un borde'),
                    'type' => 'boolean',
                ],
                'settings.border_color' => [
                    'label' => 'Color de borde',
                    'type' => 'colorpicker',
                    'show' => 'content.settings.with_border'
                ],
                'settings.border_style' => [
                    'label' => 'Estilo de borde',
                    'type' => 'select',
                    'options' => [
                        'solid' => __d('admin', 'Sólido'),
                        'dotted' => __d('admin', 'Puntos'),
                        'dashed' => __d('admin', 'Líneas'),
                    ],
                    'show' => 'content.settings.with_border'
                ],
                'settings.border_radius' => [
                    'label' => __d('admin', 'Curvatura de borde'),
                    'type' => 'select',
                    'options' => [
                        '1px' => '1px',
                        '2px' => '2px',
                        '3px' => '3px',
                        '4px' => '4px',
                        '5px' => '5px',
                        '10px' => '10px',
                        '15px' => '15px',
                        '20px' => '20px',
                        '25px' => '25px',
                        '30px' => '30px',
                        '35px' => '35px',
                    ],
                    'show' => 'content.settings.with_border'
                ],
                'settings.background_color' => [
                    'label' => __d('admin', 'Color de fondo'),
                    'type' => 'colorpicker',
                ],
                'settings.background_image' => [
                    'label' => __d('admin', 'Imagen de fondo'),
                    'type' => 'upload',
                    'config' => [
                        'type' => 'post',
                        'size' => 'thm'
                    ],
                    'template' => 'Block/fields/background_image'
                ],
                'settings.background_size' => [
                    'label' => __d('admin', 'Imagen de fondo con'),
                    'type' => 'select',
                    'options' => [
                        'no-repeat' => __d('admin', 'Normal'),
                        'repeat' => __d('admin', 'Repetido'),
                        'cover' => __d('admin', 'Completo'),
                        'fullwidth' => __d('admin', 'Ancho completo'),
                    ]
                ],
                'settings.background_position_horizontal' => [
                    'label' => __d('admin', 'Posición horizontal'),
                    'type' => 'select',
                    'options' => [
                        'left' => __d('admin', 'Izquierda'),
                        'center' => __d('admin', 'Centrada'),
                        'right' => __d('admin', 'Derecha'),
                    ]
                ],
                'settings.background_position_vertical' => [
                    'label' => __d('admin', 'Posición vertical'),
                    'type' => 'select',
                    'options' => [
                        'top' => __d('admin', 'Superior'),
                        'middle' => __d('admin', 'Centro'),
                        'bottom' => __d('admin', 'Inferior'),
                    ]
                ],
            ])

            ->addColumn('update', [
                'key' => 'general',
                'title' => __d('admin', 'Atributos'),
                'cols' => 12,
                'box' => [
                    [
                        'key' => 'attributes',
                        'elements' => $updateElements
                    ]
                ]
            ], ['position' => 'after'])

            // ->addColumn( 'update', [
            //     'key' => 'design',
            //     'title' => __d( 'admin', 'Diseño'),
            //     'cols' => 12,
            //     'box' => [
            //       [
            //         'elements' => [
            //           'settings.design',
            //         ]
            //       ]
            //     ]
            // ], ['position' => 'after']) 
        ;
    }
}
