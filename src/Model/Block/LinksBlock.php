<?php

namespace Block\Model\Block;

class LinksBlock
{

  public function parse( $Table)
  {
    $Table->hasMany( 'Links', [
      'className' => 'Section.Links',
      'foreignKey' => 'foreign_key',
      'dependent' => true
    ]);

    $Table->crud->addAssociations(['Links']);
    

    $Table->crud
      ->setName([
          'singular' => __d( 'admin', 'Enlaces'),
          'plural' => __d( 'admin', 'Enlaces'),
        ])
      ->addFields([
        'title' => __d( 'admin', 'Título'),
        'key' => [
          'type' => 'hidden'
        ],
        'links' => [
          'type' => 'hasMany',
          'label' => __d( 'admin', 'Enlaces'),
        ],
      ])
      ;

    $Table->crud->addView( 'create', [
        'saveButton' => true,
        'columns' => [
          [
            'key' => 'general',            
            'cols' => 12,
            'box' => [
              [
                'title' => null,
                'elements' => [
                  'title',
                  'links',
                ]
              ]
            ],  
          ]
        ]
      ], ['update']);

  }
}