<?php

namespace Block\Model\Block;

use Block\Model\Table\BlocksSettingsTrait;


class ContentBlock
{
  use BlocksSettingsTrait;
  
  public function parse( $Table)
  {
    // Llamando al método de BlocksSettingsTrait
    $this->_setGeneral( $Table);
  }

  public function defaults()
  {
    $data = [
      'subtype' => 'content',
      'position' => 1
    ];

    return $data;
  }
}