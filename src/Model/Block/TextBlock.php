<?php

namespace Block\Model\Block;
use I18n\Lib\Lang;



class TextBlock
{

  public function parse( $Table)
  {
    $Table->crud
      ->addFields([
        'published_at' => __d( 'admin', 'Fecha de publicación'),
        'published' => __d( 'admin', 'Publicado'),
        'body' => __d( 'admin', 'Cuerpo de texto'),
        'key' => [
          'type' => 'hidden'
        ],
      ]);


    
  }

  public function defaults()
  {
    $data = [
      'subtype' => 'text',
      'position' => 1,
      'body' => '',
    ];

    return $data;
  }
}