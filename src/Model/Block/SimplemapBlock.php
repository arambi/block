<?php

namespace Block\Model\Block;

class SimplemapBlock
{
  public function parse( $Table)
  {
    $Table->crud
      ->setName([
          'singular' => __d( 'admin', 'Mapa simple'),
          'plural' => __d( 'admin', 'Mapa simple'),
        ])
      ->addFields([
        'settings.height' => [
          'label' => __d( 'admin', 'Altura (px)'),
          'type' => 'numeric',
        ],
        'settings.styles' => [
          'label' => __d( 'admin', 'Estilos de javascript'),
          'type' => 'text',
        ],
        'simplemap' => [
          'label' => __d( 'admin', 'Mapa'),
          'type' => 'string',
          'template' => 'Block.fields/simplemap',
          'options' => [
            'fields' => [
              'lat' => 'content.settings.lat',
              'lng' => 'content.settings.lng',
              'zoom' => 'content.settings.zoom',
            ]
          ]
        ],
        'key' => [
          'type' => 'hidden'
        ]
      ]);

    $Table->crud->addView( 'create', [
        'saveButton' => true,
        'columns' => [
          [
            'cols' => 12,
            'box' => [
              [
                'title' => null,
                'elements' => [
                  'settings.height',
                  'simplemap',
                  'settings.styles',
                ]
              ]
            ],  
          ]
        ]
      ], ['update']);

    $Table->crud->defaults([
      'settings' => [
        'zoom' => 12
      ]
    ]);
  }
}