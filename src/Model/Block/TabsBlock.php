<?php

namespace Block\Model\Block;

class TabsBlock
{
  public function parse( $Table)
  {
    $Table->hasMany( 'Tabs', [
      'className' => 'Block.Tabs',
      'foreignKey' => 'parent_id'
    ]);

    $Table->crud->addAssociations( ['Tabs']);

    $Table->crud
      ->setName([
          'singular' => __d( 'admin', 'Pestanas'),
          'plural' => __d( 'admin', 'Pestanas'),
        ])
      ->addFields([
        'title' => __d( 'admin', 'Título de referencia'),
        'key' => [
          'type' => 'hidden'
        ],
        'tabs' => [
          'type' => 'hasMany',
          'label' => __d( 'admin', 'Pestañas'),
        ]
      ]);

    $Table->crud->addView( 'create', [
        'saveButton' => true,
        'columns' => [
          [
            'title' => 'General',
            'cols' => 12,
            'box' => [
              [
                'title' => null,
                'elements' => [
                  'title',
                  'tabs'
                ]
              ]
            ],  
          ]
        ]
      ], ['update']);
  }
}