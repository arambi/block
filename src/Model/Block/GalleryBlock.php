<?php

namespace Block\Model\Block;
use Block\Model\Table\BlocksSettingsTrait;

class GalleryBlock
{
  use BlocksSettingsTrait;

  public function parse( $Table)
  {
    $Table->crud
      ->setName([
          'singular' => __d( 'admin', 'Galería de fotos'),
          'plural' => __d( 'admin', 'Galería de fotos'),
        ])
      ->addFields([
        'title' => __d( 'admin', 'Título'),
        'published' => __d( 'admin', 'Publicado'),
        'title' => __d( 'admin', 'Título'),
        'key' => [
          'type' => 'hidden'
        ],
        'photo' => [
          'type' => 'uploads',
          'label' => 'Fotos',
          'config' => [
            'type' => 'gallery',
            'size' => 'thm'
          ]
        ]
      ]);

    $Table->crud->addView( 'create', [
        'saveButton' => true,
        'columns' => [
          [ 
            'title' => __d( 'admin', 'Contenido'),
            'cols' => 12,
            'box' => [
              [
                'title' => null,
                'elements' => [
                  'title',
                  'photo',
                  'published'
                ]
              ]
            ],  
          ]
        ]
      ], ['update']);

    // Llamando al método de BlocksSettingsTrait
    $this->_setGeneral( $Table);
  }
}