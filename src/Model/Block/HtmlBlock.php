<?php

namespace Block\Model\Block;

class HtmlBlock
{
  public function parse( $Table)
  {
    $Table->crud
      ->setName([
          'singular' => __d( 'admin', 'Código Html'),
          'plural' => __d( 'admin', 'Html'),
        ])
      ->addFields([
        'title' => __d( 'admin', 'Título de referencia'),
        'body' => [
          'type' => 'text',
          'label' => __d( 'admin', 'Código')
        ],
        'key' => [
          'type' => 'hidden'
        ]
      ]);

    $Table->crud->addView( 'create', [
        'saveButton' => true,
        'columns' => [
          [
            'cols' => 12,
            'box' => [
              [
                'title' => null,
                'elements' => [
                  'title',
                  'body',
                ]
              ]
            ],  
          ]
        ]
      ], ['update']);
  }
}