<?php

namespace Block\Model\Block;

use Block\Model\Table\BlocksSettingsTrait;
use Cake\ORM\TableRegistry;

class ImageBlock
{
  use BlocksSettingsTrait;

  public function parse( $Table)
  {

    $Table->crud
      ->setName([
          'singular' => __d( 'admin', 'Imagen'),
          'plural' => __d( 'admin', 'Imagen'),
        ])
      ->addFields([
        'key' => [
          'type' => 'hidden'
        ],
        'photo' => [
          'type' => 'upload',
          'label' => __d( 'admin', 'Imagen'),
          'config' => [
            'type' => 'default',
            'size' => 'thm'
          ]
        ],
        'settings.url_type' => [
          'type' => 'select',
          'label' => __d( 'admin', 'Tipo de enlace'),
          'empty' => '-- '. __d( 'admin', 'Sin enlace') . '--',
          'options' => [
            '' => '-- '. __d( 'admin', 'Sin enlace') . '--',
            'url' => __d( 'admin', 'Un enlace escrito'),
            'section' => __d( 'admin', 'Una sección'),
            'file' => 'Un fichero'
          ]
        ],
        'docs' => [
          'type' => 'upload',
          'label' => __d('admin', 'Fichero'),
          'config' => [
              'type' => 'doc',
          ],
          'show' => "content.settings.url_type == 'file'"
      ],
        'settings.target_blank' => [
          'label' => 'Abrir en una ventana nueva',
          'type' => 'boolean'
        ],
        'subtitle' => [
          'type' => 'string',
          'label' => __d( 'admin', 'Enlace'),
          'show' => 'content.settings.url_type == "url"',
        ],
        'settings.align' => [
          'type' => 'select',
          'label' => __d( 'admin', 'Alineación'),
          'options' => [
            'none' => __d( 'admin', 'Por defecto'),
            'left' => __d( 'admin', 'Izquierda'),
            'center' => __d( 'admin', 'Centrada'),
            'right' => __d( 'admin', 'Derecha'),
          ]
        ],
        'parent_id' => [
          'label' => __d( 'admin', 'Enlace a sección'),
          'type' => 'select',
          'options' => function( $crud){
            return TableRegistry::get( 'Section.Sections')->selectOptions();
          },
          'show' => 'content.settings.url_type == "section"',
        ]
      ]);

    $Table->crud->addView( 'create', [
        'saveButton' => true,
        'columns' => [
          [
            'title' => __d( 'admin', 'Imagen'),
            'cols' => 12,
            'box' => [
              [
                'elements' => [
                  'settings.target_blank',
                  'settings.url_type',
                  'subtitle',
                  'parent_id',
                  'docs',
                  'settings.align',
                  'key',
                  'photo'
                ]
              ]
            ],  
          ]
        ]
      ], ['update']);

    // Llamando al método de BlocksSettingsTrait
    // $this->_setGeneral( $Table);
  }
}