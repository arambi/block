<?php

namespace Block\Model\Block;
  
use Cake\ORM\TableRegistry;
use Block\Model\Table\BlocksSettingsTrait;


/**
 * Usado por aquellos behaviors que usan JSON para guardar datos, como JsonableBehavior y UploadJsonableBehavior
 */
trait BlockTrait
{

  public function parseModel( $contentBlock)
  {
    $BlocksTable = TableRegistry::get( 'Block.Blocks');
    $Block = new $contentBlock->block_type ['className'];
    $Block->parse( $BlocksTable);
    return $BlocksTable;
  }
}
