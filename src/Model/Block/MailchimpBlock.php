<?php

namespace Block\Model\Block;

class MailchimpBlock
{
  public function parse( $Table)
  {
    $Table->crud
      ->setName([
          'singular' => __d( 'admin', 'Mailchimp'),
          'plural' => __d( 'admin', 'Mailchimp'),
        ])
      ->addFields([
        'title' => [
          'label' => __d( 'admin', 'Texto para el campo'),
          'help' => __d( 'admin', 'Introduce un texto para el campo de input')
        ],
        'settings.action' => [
          'label' => __d( 'admin', 'URL de envio de formulario'),
          'type' => 'string',
          'help' => __d( 'admin', 'Introduce la URL a donde se enviará el formulario')
        ],
        'settings.inputcode' => [
          'label' => __d( 'admin', 'Nombre del campo de email'),
          'type' => 'string',
          'help' => __d( 'admin', 'Introduce el nombre del campo del email (es un código codificado)')
        ],
        'key' => [
          'type' => 'hidden'
        ]
      ]);

    $Table->crud->addView( 'create', [
        'saveButton' => true,
        'columns' => [
          [
            'cols' => 12,
            'box' => [
              [
                'title' => null,
                'elements' => [
                  'title',
                  'settings.action',
                  'settings.inputcode'
                ]
              ]
            ],  
          ]
        ]
      ], ['update']);
  }
}