<?php

namespace Block\Model\Block;

class SocialContentBlock
{
  public function parse( $Table)
  {
    $Table->crud
      ->setName([
        'singular' => __d( 'admin', 'Contenido Social'),
        'plural' => __d( 'admin', 'Contenido Social'),
      ])
      ->addFields([
        'title' => __d( 'admin', 'Título'),
        'key' => [
          'type' => 'hidden'
        ],
        
        'settings.all' => [
          'label' => __d( 'admin', 'Mostrar todas en un solo bloque'),
          'type' => 'boolean'
        ],

        // Facebook
        'settings.facebook' => [
          'label' => __d( 'admin', 'Mostrar'),
          'type' => 'boolean',
        ],
        'settings.facebook_title' => [
          'label' => __d( 'admin', 'Subtítulo'),
          'type' => 'string',
          'show' => 'content.settings.facebook'
        ],
        'settings.facebook_url' => [
          'label' => __d( 'admin', 'URL del perfil'),
          'type' => 'string',
          'show' => 'content.settings.facebook'
        ],
        'settings.facebook_fan_count' => [
          'label' => __d( 'admin', 'Mostrar número de fans'),
          'type' => 'boolean',
          'show' => 'content.settings.facebook'
        ],
        'settings.facebook_show_posts' => [
          'label' => __d( 'admin', 'Mostrar publicaciones'),
          'type' => 'boolean',
          'help' => __d( 'admin', 'Al marcar esta opción se mostrarán las publicaciones del usuario'),
          'show' => 'content.settings.facebook'
        ],
        'settings.facebook_id' => [
          'type' => 'string',
          'label' => __d( 'admin', 'Id de usuario'),                    
          'show' => 'content.settings.facebook'
        ],
        'settings.facebook_limit' => [
          'label' => 'Límite',
          'type' => 'numeric',
          'range' => [1, 50],
          'show' => 'content.settings.facebook && content.settings.facebook_show_posts'
        ],

        // Twitter
        'settings.twitter' => [
          'label' => __d( 'admin', 'Mostrar'),
          'type' => 'boolean',
        ],
        'settings.twitter_title' => [
          'label' => __d( 'admin', 'Subtítulo'),
          'type' => 'string',
          'help' => __d( 'admin', 'Por lo general, el nombre de usuario en la red social, tipo @nombre_usuario'),
          'show' => 'content.settings.twitter'
        ],
        'settings.twitter_url' => [
          'label' => __d( 'admin', 'URL del perfil'),
          'type' => 'string',
          'show' => 'content.settings.twitter'
        ],
        'settings.twitter_show_posts' => [
          'label' => __d( 'admin', 'Mostrar publicaciones'),
          'type' => 'boolean',
          'help' => __d( 'admin', 'Al marcar esta opción se mostrarán las publicaciones del usuario'),
          'show' => 'content.settings.twitter'
        ],
        'settings.twitter_id' => [
          'type' => 'string',
          'label' => __d( 'admin', 'Id de usuario'),                    
          'show' => 'content.settings.twitter && content.settings.twitter_show_posts'
        ],
        'settings.twitter_limit' => [
          'label' => 'Límite',
          'type' => 'numeric',
          'range' => [1, 50],
          'show' => 'content.settings.twitter && content.settings.twitter_show_posts'
        ],

        // Youtube
        'settings.youtube' => [
          'label' => __d( 'admin', 'Mostrar'),
          'type' => 'boolean',
        ],
        'settings.youtube_url' => [
          'label' => __d( 'admin', 'URL del perfil'),
          'type' => 'string',
          'show' => 'content.settings.youtube'
        ],
        'settings.youtube_show_posts' => [
          'label' => __d( 'admin', 'Mostrar publicaciones'),
          'type' => 'boolean',
          'help' => __d( 'admin', 'Al marcar esta opción se mostrarán las publicaciones del usuario'),
          'show' => 'content.settings.youtube'
        ],
        'settings.youtube_id' => [
          'type' => 'string',
          'label' => __d( 'admin', 'Id de usuario'),                    
          'show' => 'content.settings.youtube && content.settings.youtube_show_posts'
        ],
        'settings.youtube_limit' => [
          'label' => 'Límite',
          'type' => 'numeric',
          'range' => [1, 50],
          'show' => 'content.settings.youtube && content.settings.youtube_show_posts'
        ],

        // Instagram
        'settings.instagram' => [
          'label' => __d( 'admin', 'Mostrar'),
          'type' => 'boolean',
        ],
        
        'settings.instagram_title' => [
          'label' => __d( 'admin', 'Subtítulo'),
          'type' => 'string',
          'help' => __d( 'admin', 'Por lo general, el nombre de usuario en la red social, tipo @nombre_usuario')
        ],
        'settings.instagram_url' => [
          'label' => __d( 'admin', 'URL del perfil'),
          'type' => 'string',
          'show' => 'content.settings.instagram'
        ],
        'settings.instagram_show_posts' => [
          'label' => __d( 'admin', 'Mostrar publicaciones'),
          'type' => 'boolean',
          'help' => __d( 'admin', 'Al marcar esta opción se mostrarán las publicaciones del usuario'),
          'show' => 'content.settings.instagram'
        ],
        'settings.instagram_login' => [
          'label' => __d( 'admin', 'Login (email)'),
          'type' => 'string',
          'help' => __d( 'admin', 'El login en Instagram'),
          'show' => 'content.settings.instagram_show_posts'
        ],
        'settings.instagram_password' => [
          'label' => __d( 'admin', 'Contraseña'),
          'type' => 'string',
          'help' => __d( 'admin', 'La contraseña en Instagram'),
          'show' => 'content.settings.instagram_show_posts'
        ],
        'settings.instagram_limit' => [
          'label' => 'Límite',
          'type' => 'numeric',
          'range' => [1, 20],
          'show' => 'content.settings.instagram_show_posts'
        ],
        // Ivoox
        'settings.ivox' => [
          'label' => __d( 'admin', 'Mostrar'),
          'type' => 'boolean',
        ],
        'settings.ivox_url' => [
          'label' => __d( 'admin', 'URL del perfil'),
          'type' => 'string',
          'show' => 'content.settings.ivox'
        ],
        'settings.ivox_show_posts' => [
          'label' => __d( 'admin', 'Mostrar publicaciones'),
          'type' => 'boolean',
          'help' => __d( 'admin', 'Al marcar esta opción se mostrarán las publicaciones del usuario'),
          'show' => 'content.settings.ivox'
        ],
        'settings.ivox_id' => [
          'type' => 'string',
          'label' => __d( 'admin', 'Código Iframe'),                    
          'show' => 'content.settings.ivox && content.settings.ivox_show_posts'
        ],        
        'settings.ivox_limit' => [
          'label' => 'Límite',
          'type' => 'numeric',
          'range' => [1, 50],
          'show' => 'content.settings.youtube && content.settings.youtube_show_posts'
        ],

        // Tripadvisor
        'settings.tripadvisor' => [
          'label' => __d( 'admin', 'Mostrar'),
          'type' => 'boolean',
        ],
        'settings.tripadvisor_url' => [
          'label' => __d( 'admin', 'URL del perfil'),
          'type' => 'string',
          'show' => 'content.settings.tripadvisor'
        ],
      ]);
    
    $Table->crud->addView( 'create', [
        'saveButton' => true,
        'columns' => [
          [
            'cols' => 12,
            'box' => [
              [
                'title' => null,
                'elements' => [
                  'title',
                  'settings.all',
                ]
              ],
              [
                'title' => 'Twitter',
                'elements' => [
                  'settings.twitter',
                  'settings.twitter_url',
                  'settings.twitter_title',
                  'settings.twitter_show_posts',
                  'settings.twitter_id',
                  'settings.twitter_limit',
                ]
              ],
              [
                'title' => 'Facebook',
                'elements' => [
                  'settings.facebook',
                  'settings.facebook_title',
                  'settings.facebook_url',
                  'settings.facebook_fan_count',
                  'settings.facebook_show_posts',
                  'settings.facebook_id',
                  'settings.facebook_limit',
                ]
              ],
              [
                'title' => 'Youtube',
                'elements' => [
                  'settings.youtube',
                  'settings.youtube_url',
                  'settings.youtube_show_posts',
                  'settings.youtube_id',
                  'settings.youtube_limit',
                ]
              ],
              [
                'title' => 'Instagram',
                'elements' => [
                  'settings.instagram',
                  'settings.instagram_title',
                  'settings.instagram_url',
                  'settings.instagram_show_posts',
                  'settings.instagram_login',
                  'settings.instagram_password',
                  'settings.instagram_limit',
                ]
              ],
              [
                'title' => 'Ivoox',
                'elements' => [
                  'settings.ivox',
                  'settings.ivox_url',
                  'settings.ivox_show_posts',
                  'settings.ivox_id',
                  'settings.youtube_limit',
                ]
              ],
              [
                'title' => 'TripAdvisor',
                'elements' => [
                  'settings.tripadvisor',
                  'settings.tripadvisor_url',
                ]
              ]
            ],  
          ]
        ]
      ], ['update']);
  }
}