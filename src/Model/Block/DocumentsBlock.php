<?php

namespace Block\Model\Block;

class DocumentsBlock
{
  public function parse( $Table)
  {
    $Table->crud
      ->setName([
          'singular' => __d( 'admin', 'Adjuntos'),
          'plural' => __d( 'admin', 'Adjuntos'),
        ])
      ->addFields([
        'title' => __d( 'admin', 'Título'),
        'key' => [
          'type' => 'hidden'
        ],
        'docs' => [
          'label' => __d( 'admin', 'Ficheros'),
          'type' => 'uploads',
          'config' => [
            'type' => 'doc',
            'langs_multiple' => true,
          ]
        ]
      ]);

    $Table->crud->addView( 'create', [
        'saveButton' => true,
        'columns' => [
          [
            'cols' => 12,
            'box' => [
              [
                'title' => null,
                'elements' => [
                  'title',
                  'docs'
                ]
              ]
            ],  
          ]
        ]
      ], ['update']);
  }
}