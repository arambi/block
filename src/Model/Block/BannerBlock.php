<?php

namespace Block\Model\Block;

class BannerBlock
{
  public function parse( $Table)
  {
    use BlocksSettingsTrait;

    $Table->crud
      ->setName([
          'singular' => __d( 'admin', 'Banner'),
          'plural' => __d( 'admin', 'Banners'),
        ])
      ->addFields([
        'title' => __d( 'admin', 'Título'),
        'subtitle' => [
          'label' => __d( 'admin', 'Texto del botón'),
          'show' => 'content.settings.with_button'
        ],
        'url' => [
          'label' => __d( 'admin', 'URL'),
          'show' => 'content.settings.with_button'
        ],
        'settings.with_button' => [
          'label' => 'Con botón',
          'type' => 'boolean'
        ],
        'body' => [
          'label' => __d( 'admin', 'Texto'),
          'type' => 'ckeditor',
        ],
        'photo' => [
          'type' => 'upload',
          'label' => __d( 'admin', 'Imagen'),
          'config' => [
            'type' => 'gallery',
            'size' => 'thm'
          ]
        ],
        'key' => [
          'type' => 'hidden'
        ]
      ])

    ;

    $Table->crud->addView( 'create', [
        'saveButton' => true,
        'columns' => [
          [
            'cols' => 12,
            'box' => [
              [
                'title' => null,
                'elements' => [
                  'title',
                  'body',
                  'settings.with_button',
                  'subtitle',
                  'url',
                ]
              ]
            ],  
          ]
        ]
      ], ['update']);

    // Llamando al método de BlocksSettingsTrait
    $this->_setGeneral( $Table);
  }
}