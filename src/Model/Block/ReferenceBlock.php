<?php

namespace Block\Model\Block;

use Cake\Utility\Text;

class ReferenceBlock
{
  public function parse( $Table)
  {
    $Table->crud
      ->setName([
          'singular' => __d( 'admin', 'Bloque existente'),
          'plural' => __d( 'admin', 'Bloque existente'),
        ])
      ->addFields([
        'title' => __d( 'admin', 'Título'),
        'parent_id' => [
          'label' => 'Bloque',
          'type' => 'select',
          'options' => function() use ( $Table){
            $blocks = $Table->find()
              ->where(function( $exp, $query){
                return $exp->not([
                  'subtype IN' => ['content', 'reference']
                ]);
              })
              ->order([
                'Blocks.subtype'
              ]);

            $options = [];

            foreach( $blocks as $block)
            {
              if( $block->subtype == 'text')
              {
                continue;
                // $_title = trim( Text::truncate( strip_tags( $block->body), 50));
              }
              else
              {
                $_title = $block->title;
              }

              $title = $block->block_type ['title'];

              if( !empty( $_title))
              {
                $title .= ' - '. $_title;
              }

              $content_title = $block->getContentTitle();

              if ($content_title) {
                $title .= ' - '. $content_title;
              }
              
              $options [$block->id] = $title;

            }

            return $options;


          }
        ],
        'key' => [
          'type' => 'hidden'
        ]
      ]);

    $Table->crud->addView( 'create', [
        'saveButton' => true,
        'columns' => [
          [
            'cols' => 12,
            'box' => [
              [
                'title' => null,
                'elements' => [
                  'parent_id'
                ]
              ]
            ],  
          ]
        ]
      ], ['update']);
  }
}