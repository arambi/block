<?php

namespace Block\Model\Block;

class VideoBlock
{
  public function parse( $Table)
  {
    $Table->crud
      ->setName([
          'singular' => __d( 'admin', 'Video'),
          'plural' => __d( 'admin', 'Video'),
        ])
      ->addFields([
        'title' => __d( 'admin', 'Título'),
        'video' => __d( 'admin', 'URL'),
        'key' => [
          'type' => 'hidden'
        ],
        'special' => __d( 'admin', 'Usar una imagen de portada (en lugar de la de Youtube)'),
        'photo' => [
          'type' => 'upload',
					'label' => __d( 'admin', 'Foto de portada'),
					'config' => [
						'type' => 'default',
						'size' => 'thm'
          ],
          'show' => 'content.special'
        ]
      ]);

    $Table->crud->addView( 'create', [
        'saveButton' => true,
        'columns' => [
          [
            'cols' => 12,
            'box' => [
              [
                'title' => null,
                'elements' => [
                  'title',
                  'video',
                  'special',
                  'photo',
                ]
              ]
            ],  
          ]
        ]
      ], ['update']);
  }
}