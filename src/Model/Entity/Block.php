<?php

namespace Block\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cake\ORM\Behavior\Translate\TranslateTrait;
use Manager\Model\Entity\CrudEntityTrait;
use Cofree\Model\Entity\VideoEntityTrait;
use Blog\Model\Entity\BlogEntityTrait;
use Cake\Core\Plugin;
use Cake\Event\EventManager;
use Cake\Event\Event;

/**
 * Block Entity.
 */
class Block extends Entity
{
    use CrudEntityTrait;
    use TranslateTrait;
    use BlogEntityTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'content_type' => true,
        'site_id' => true,
        'parent_id' => true,
        'category_id' => true,
        'slug' => true,
        'published' => true,
        'published_at' => true,
        'subtype' => true,
        'title' => true,
        'antetitle' => true,
        'subtitle' => true,
        'body' => true,
        'settings' => true,
        'url' => true,
        'photo' => true,
        'site' => true,
        'parent_block' => true,
        'category' => true,
        'content_categories' => true,
        'child_blocks' => true,
        'sections' => true,
        'cols' => true,
        'tabs' => true,
        'position' => true,
        'block_type' => true,
        'links' => true,
    ];

    public function __construct(array $properties = [], array $options = [])
    {
        parent::__construct($properties, $options);
        $event = new Event('Block.Model.Entity.Block.construct', $this);
        EventManager::instance()->dispatch($event);
    }

    public function link()
    {
        $row = $this->getRow();

        if (!$row) {
            return null;
        }

        if ($row->model == 'Sections') {
            $url = TableRegistry::getTableLocator()->get('Section.Sections')->getUrl($row->content_id);

            if ($url) {
                return $url;
            }
        }
    }

    public function getRow()
    {
        $col = TableRegistry::getTableLocator()->get('Block.Columns')->find()
            ->where([
                'Columns.id' => $this->category_id
            ])
            ->first();

        if (!$col) {
            return null;
        }

        $row = TableRegistry::getTableLocator()->get('Block.Rows')->find()
            ->where([
                'Rows.id' => $col->row_id
            ])
            ->first();

        return $row;
    }

    public function getModel()
    {
        $row = $this->getRow();

        if (!$row) {
            return null;
        }

        $model = $row->model;
        $plugins = Plugin::loaded();

        $locator = TableRegistry::getTableLocator();

        foreach ($plugins as $plugin) {
            if ($locator->exists("$plugin.$model")) {
                return $locator->get("$plugin.$model");
            }
        }
    }

    public function getContent()
    {
        $row = $this->getRow();
        $model = $this->getModel();

        if ($model) {
            $content = $model->find()
                ->where([
                    $model->getAlias() . '.' . $model->getPrimaryKey() => $row->content_id
                ])
                ->first();

            return $content;
        }
    }

    public function getContentTitle()
    {
        $content = $this->getContent();
        
        if ($content) {
            $model = $this->getModel();
            return $content->get($model->getDisplayField());
        }
    }

    public function blockLink($title = null, $only_url = false)
    {
        if ($title === null) {
            $title = $this->title;
        }

        if (is_object($this->settings) && @$this->settings->link_type == 'section') {
            $url = TableRegistry::getTableLocator()->get('Section.Sections')->getUrl($this->parent_id);
        } elseif (is_object($this->settings) && @$this->settings->link_type == 'url') {
            $url = $this->url;
        } elseif (is_object($this->docs) && @$this->settings->link_type == 'file') {
            $url = $this->docs->paths[0];
        }

        if (!empty($url)) {
            if ($only_url) {
                return $url;
            }

            $target = is_object($this->settings) && !empty($this->settings->target_blank) ? ' target="_blank"' : '';
            return '<a href="' . $url . '"' . $target . ' title="' . strip_tags($title) . '">' . $title . '</a>';
        }
    }

    public function saveSearch($locale)
    {
        $block = TableRegistry::getTableLocator()->get('Block.Blocks')->getBlockObject($this->subtype);

        if (method_exists($block, 'saveSearch')) {
            return $block->saveSearch($this, $locale);
        }
    }
}
