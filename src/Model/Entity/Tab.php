<?php
namespace Block\Model\Entity;

use Manager\Model\Entity\CrudEntityTrait;
use Cake\ORM\Behavior\Translate\TranslateTrait;use Cake\ORM\Entity;

/**
 * Tab Entity.
 *
 * @property int $id
 * @property string $content_type
 * @property int $site_id
 * @property int $parent_id
 * @property string $subtype
 * @property int $category_id
 * @property bool $published
 * @property \Cake\I18n\Time $published_at
 * @property string $title
 * @property string $antetitle
 * @property string $subtitle
 * @property string $summary
 * @property string $body
 * @property string $url
 * @property int $row
 * @property int $cols
 * @property int $position
 * @property string $settings
 * @property string $salt
 * @property string $photo
 * @property string $photos
 * @property string $docs
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property bool $draft
 */
class Tab extends Entity
{
    use CrudEntityTrait;
    use TranslateTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
