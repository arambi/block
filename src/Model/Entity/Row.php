<?php
namespace Block\Model\Entity;

use Cake\ORM\Entity;
use Manager\Model\Entity\CrudEntityTrait;

/**
 * Row Entity.
 */
class Row extends Entity 
{

  use CrudEntityTrait;
  
/**
 * Fields that can be mass assigned using newEntity() or patchEntity().
 *
 * @var array
 */
	protected $_accessible = [
		'*' => true,
	];

	public function hasContent()
	{
		foreach( $this->columns as $column)
		{
			foreach( $column->blocks as $block)
			{
				if( $block->subtype == 'content')
				{
					return true;
				}
			}
		}

		return false;
	}
}
