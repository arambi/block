<?php 

namespace Block\Model\Entity;

trait BlockTrait
{
  protected function _getFullwidth()
  {
    foreach( $this->rows as $row)
    {
      if( @$row->settings->fullwidth)
      {
        return true;
      }
    }

    return false;
  }
}