<?php
namespace Block\View\Cell;

use Cake\View\Cell;
use Cake\Cache\Cache;
use Madcoda\Youtube\Youtube;
use PicoFeed\Reader\Reader;


/**
 * SocialContent cell
 */
class SocialContentCell extends Cell
{

  /**
   * List of valid options that can be passed into this
   * cell's constructor.
   *
   * @var array
   */
  protected $_validCellOptions = [];

  private $__twitter = [
    'appKey' =>  'g24aNQ2YgHpDd8wL1h1RhBGo3',
    'appSecret' => 'YaASDLw8znjkNrmLLXdOLpUFYxqAZ0fyosppL94IUG8n9DhS23',
    'token' => '2475972362-1JXSuK8qlS6ROjXYmZG2CkbBcpJTZqsv0FXQe8L',
    'tokenSecret' => 'IRJtahw8uy9q02QpmMukOCVbpdVH4ntLpzYVjTZpWBqXp',
  ];

  private $__youtube = [
    // 'appKey' =>  'AIzaSyBm5eOZYg4Btcc186Rvzum-vA4fGihVRFI',
    'appKey' =>  'AIzaSyDvmpOzEO6FjjKLP_Je4cP_qwJE6QAZ6FY',
  ];

  private $__providers = [
    'twitter',
    'facebook',
    'youtube',
    'instagram',
    'ivox',
    'tripadvisor'
  ];

  /**
   * Default display method.
   *
   * @return void
   */
  public function display( $block)
  {
    $results = [];

    foreach( $this->__providers as $provider)
    {
      if( !empty( $block->settings->$provider))
      {
        $results [$provider] = $this->get( $provider, $block);
      }
    }
    
    if( !empty( $block->settings->all))
    {
      $results = $this->mergeAll( $results);
      $results = array_values( $results);
    }

    $block->set( 'results', $results);
    $this->set( compact( 'block'));
  }

  private function get( $provider, $block)
  {
    $keyCache = $provider . $block->{$provider . '_id'} . $block->id;

    $results = Cache::read( $keyCache);

    if( !$results)
    {
      $method = '_get'. ucfirst( $provider);
      $results = $this->$method( $block);
      Cache::write( $keyCache, $results);
    }

    return $results;
  }

  private function mergeAll( $results)
  {
    $posts = [];

    foreach( $results as $provider => $_posts)
    {
      foreach( (array)$_posts ['posts'] as $post)
      {
        $post ['created_at'] = strtotime( $post ['created_at']);
        $posts [] = $post;
      }
    }

    $posts = collection( $posts)->sortBy( 'created_at', SORT_DESC)->toArray();

    foreach( $posts as &$post)
    {
      $post ['created_at'] = date( 'd-m-Y H:i:s', $post ['created_at']);
    }

    return $posts;
  }

  protected function _getTwitter( $block)
  {
    try {
      $twitter = new \Twitter( $this->__twitter ['appKey'], $this->__twitter ['appSecret'], $this->__twitter ['token'], $this->__twitter ['tokenSecret']);

      $timeline = $twitter->request( '/statuses/user_timeline', 'GET', [
        'screen_name' => $block->settings->twitter_id,
        'count' => $block->settings->twitter_limit,
        'include_rts' => false,
        'include_entities' => false
      ]);
    } catch (\TwitterException $e) {
      return false;
    }

    $data = [];

    foreach( $timeline as $line)
    {
      $data [] = [
        'created_at' => $this->date( $line->created_at),
        'type' => 'text',
        'text' => $this->parseTwitter( $line->text),
        'provider' => 'twitter'
      ];
    }
    return [
      'posts' => $data
    ];
  }

  protected function _getYoutube( $block)
  {
    $youtube = new Youtube( array('key' => $this->__youtube ['appKey']));
    $timeline = $youtube->searchChannelVideos( '', $block->settings->youtube_id, $block->settings->youtube_limit, 'date');

    foreach( $timeline as $line)
    {
      $video = $youtube->getVideoInfo( $line->id->videoId);
      $data [] = [
        'id' => $line->id->videoId,
        'created_at' => $this->date( $video->snippet->publishedAt),
        'type' => 'video',
        'link' => 'http://www.youtube.com/watch?v='. $line->id->videoId,
        'title' => $video->snippet->title,
        'text' => $video->snippet->description,
        'image'=> $video->snippet->thumbnails->medium->url,
        'duration' => $this->youtubeDuration( $video->contentDetails->duration),
        'views' => $video->statistics->viewCount,
        'provider' => 'youtube'
      ];
    }
    return [
      'posts' => $data
    ];
  }

/**
 * Toma los datos de las publicaciones de Facebook
 * 
 * @return array
 */
  protected function _getFacebook( $block)
  {
    $data = [];
    return $data;
  }

  protected function _getIvox( $block)
  {
    $reader = new Reader;
    $resource = $reader->download( $block->settings->ivox_id);

    $parser = $reader->getParser(
      $resource->getUrl(),
      $resource->getContent(),
      $resource->getEncoding()
    );

    $feed = $parser->execute();
    $results = $feed->getItems();

    $data = [];

    foreach( $results as $key => $result)
    {
      $data [] = [
        'type' => 'audio',
        'title' => $result->title,
        'link' => $result->url,
        'file' => $result->enclosureUrl,
        'text' => $result->content,
        'provider' => 'ivox'
      ];

      if( $key >= $block->settings->ivox_limit)
      {
        break;
      }
    }

    return [
      'posts' => $data
    ];
  }

  protected function _getTripadvisor( $block)
  {
    return [];
  }

  protected function _getInstagram( $block)
  {
    return [];
    // return [
    //   'iframe' => $block->settings->instagram_id
    // ];
  }

  private function __getInstagramId( $block, $ig)
  {
    try {
      $url = parse_url( $block->settings->instagram_url);
      $url = str_replace( '/', '', $url ['path']);
      $items = $ig->people->search( $url)->getUsers();

      foreach( $items as $item)
      {
        return $item->getPk();
      }

    } catch (\Exception $e) {
      \Cake\Log\Log::debug( 'instagram id'.  $e->getMessage());
    }
  }
  
  private function date( $date)

  {
    if( is_numeric( $date))
    {
      return date( 'd-m-Y H:i', $date);
    }

    return date( 'd-m-Y H:i', strtotime( $date));
  }

  private function safeText( $text)
  {
    $text = strip_tags( $text);
    return $text;
  }

/**
 * Parsea el string del texto de descripcion de Twitter. Le añade enlaces.
 * 
 * @param  string $ret
 * @return string
 */
  private function parseTwitter( $ret) 
  {
    $ret = preg_replace("#(^|[\n ])([\w]+?://[\w]+[^ \"\n\r\t< ]*)#", "\\1<a href=\"\\2\" target=\"_blank\">\\2</a>", $ret);
    $ret = preg_replace("#(^|[\n ])((www|ftp)\.[^ \"\t\n\r< ]*)#", "\\1<a href=\"http://\\2\" target=\"_blank\">\\2</a>", $ret);
    $ret = preg_replace("/@(\w+)/", "<a href=\"http://www.twitter.com/\\1\" target=\"_blank\">@\\1</a>", $ret);
    $ret = preg_replace("/#(\w+)/", "<a href=\"http://twitter.com/search?q=\\1\" target=\"_blank\">#\\1</a>", $ret);
    return $ret;
  }

  /**
 * Devuelve la duración de un video en el formato M::SS
 * 
 * @param  string $time El formato de duración de video dado por el API de Youtube
 * @return string
 */
  private function youtubeDuration( $time)
  {
    $duration = str_replace( 'S', '', str_replace( 'M', ':', str_replace( 'PT', '', $time)));

    if( strpos( $duration, ':') === false)
    {
      $duration = '00:'. $duration;
    }

    return $duration;
  }

/**
 * Parsea el string del texto de descripcion de Facebook. Le añade enlaces.
 * 
 * @param  string $ret
 * @return string
 */
  private function parseFacebook( $ret) 
  {
    $ret = preg_replace("#(^|[\n ])([\w]+?://[\w]+[^ \"\n\r\t< ]*)#", "\\1<a href=\"\\2\" target=\"_blank\">\\2</a>", $ret);
    $ret = preg_replace("#(^|[\n ])((www|ftp)\.[^ \"\t\n\r< ]*)#", "\\1<a href=\"http://\\2\" target=\"_blank\">\\2</a>", $ret);
    return $ret;
  }
}
