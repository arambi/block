<?php
namespace Block\View\Cell;

use Cake\View\Cell;

/**
 * Row cell
 */
class RowCell extends Cell
{

  /**
   * List of valid options that can be passed into this
   * cell's constructor.
   *
   * @var array
   */
  protected $_validCellOptions = [];

  /**
   * Default display method.
   *
   * @return void
   */
  public function display( $rows, $view = null)
  {
    $this->set( compact( 'rows'));

    if( $view !== null)
    {
      $this->template = $view;
    }
  }
}
