<?php

namespace Block\View\Cell;

use Cake\View\Cell;
use Cake\ORM\TableRegistry;

/**
 * Tabs cell
 */
class TabsCell extends Cell
{

    /**
     * List of valid options that can be passed into this
     * cell's constructor.
     *
     * @var array
     */
    protected $_validCellOptions = [];

    /**
     * Default display method.
     *
     * @return void
     */
    public function display($block)
    {
        $tabs = $this->getTableLocator()->get('Block.Tabs')->find()
            ->where([
                'Tabs.parent_id' => $block->id
            ])
            ->order([
                'Tabs.position'
            ]);

        $this->set(compact('tabs', 'block'));
    }
}
