<?php
namespace Block\View\Cell;

use Cake\View\Cell;

/**
 * Links cell
 */
class LinksCell extends Cell
{

  /**
   * List of valid options that can be passed into this
   * cell's constructor.
   *
   * @var array
   */
  protected $_validCellOptions = [];

  /**
   * Default display method.
   *
   * @return void
   */
  public function display( $block)
  {
    $contents = $this->loadModel( 'Section.Links')->find()
      ->where([
        'Links.foreign_key' => $block->id
      ]);
    
    $this->set( compact( 'block', 'contents'));
  }
}
