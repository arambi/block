<?php
namespace Block\View\Cell;

use Cake\View\Cell;

/**
 * Html cell
 */
class HtmlCell extends Cell
{

    /**
     * List of valid options that can be passed into this
     * cell's constructor.
     *
     * @var array
     */
    protected $_validCellOptions = [];

    /**
     * Default display method.
     *
     * @return void
     */
    public function display( $block)
    {
        $this->set( compact( 'block'));
    }
}
