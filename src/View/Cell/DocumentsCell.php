<?php
namespace Block\View\Cell;

use Cake\View\Cell;

/**
 * Documents cell
 */
class DocumentsCell extends Cell
{

  /**
   * List of valid options that can be passed into this
   * cell's constructor.
   *
   * @var array
   */
  protected $_validCellOptions = [];

  private $icons = [
    'doc' => 'fa fa-file-word-o',
    'xdoc' => 'fa fa-file-word-o',
    'xls' => 'fa fa-file-excel-o',
    'ppt' => 'fa fa-file-powerpoint-o',
    'xppt' => 'fa fa-file-powerpoint-o',
    'zip' => 'fa fa-file-zip-o',
    'pdf' => 'fa fa-file-pdf-o',
    'jpg' => 'fa fa-file-image-o',
    'png' => 'fa fa-file-image-o',
    'jpeg' => 'fa fa-file-image-o',
  ];

  /**
   * Default display method.
   *
   * @return void
   */
  public function display( $block)
  {
    foreach( $block->docs as $doc)
    {
      if( array_key_exists( $doc->extension, $this->icons))
      {
        $doc->set( 'icon', $this->icons [$doc->extension]);
      }
    }

    $this->set( compact( 'block'));
  }
}
