<?php

namespace Block\View\Cell;

use Cake\View\Cell;


/**
 * Image cell
 */
class ImageCell extends Cell
{

    /**
     * List of valid options that can be passed into this
     * cell's constructor.
     *
     * @var array
     */
    protected $_validCellOptions = [];

    /**
     * Default display method.
     *
     * @return void
     */
    public function display($block)
    {
        if (!empty($block->settings->url_type)) {
            if ($block->settings->url_type == 'section') {
                $this->loadModel('Section.Sections');

                $url = $this->Sections->getUrl($block->parent_id);

                if ($url) {
                    $block->set('url', $url);
                }
            } elseif (!empty($block->subtitle)) {
                $block->set('url', $block->subtitle);
            } elseif ($block->settings->url_type == 'doc' || $block->settings->url_type == 'file') {
                $block->set('url', $block->docs->paths[0]);
            }
        }

        $this->set(compact('block'));
    }
}
