<?php

namespace Block\View\Helper;

use \ArrayObject;
use Cake\View\View;
use Cake\View\Helper;
use Cake\Core\Configure;
use Cake\View\StringTemplateTrait;
use Cofree\View\Helper\StylesHelperTrait;

/**
 * Blocks helper
 */
class BlockersHelper extends Helper
{
    use StringTemplateTrait;
    use StylesHelperTrait;

    /**
     * Template de estilos 
     * El key es el nombre en la base de datos y el value es el template de cómo se escribirá
     * Si el template es tipo función, en lugar de hacer un str_replace, se llamará a esa función
     * @var [type]
     */
    public $styles = [
        'margin_top' => 'margin-top: %spx',
        'margin_right' => 'margin-right: %spx',
        'margin_bottom' => 'margin-bottom: %spx',
        'margin_left' => 'margin-left: %spx',
        'border_top_width' => 'border-top-width: %spx',
        'border_right_width' => 'border-right-width',
        'border_bottom_width' => 'border-bottom-width: %spx',
        'padding_top' => 'padding-top: %spx',
        'padding_right' => 'padding-right: %spx',
        'padding_bottom' => 'padding-bottom: %spx',
        'padding_left' => 'padding-left: %spx',
        'background_color' => 'background-color: %s',
        'border_color' => 'borderColor()',
        'border_style' => 'borderStyle()',
        'border_radius' => 'borderRadius()',
        'background_size' => 'backgroundSize()',
        'background_image' => 'backgroundImage()',
    ];


    /**+
     * Devuelve los atributos de un block, es decir, el class y el style
     * 
     * @param  Block\Model\Entity\Block $block
     * @return string HTML        
     */
    public function attrs($block, $attrs = [], $type = 'block', $key = false)
    {
        $_defaults = [
            'class' => '',
            'style' => '',
            'blockClass' => 'block',
            'classPrefix' => 'block'
        ];

        $attrs = array_merge($_defaults, $attrs);


        if (!empty($block->subtype)) {
            $attrs['class'] .= $attrs['blockClass'] . ' ' . $attrs['classPrefix'] . '-' . $block->subtype;
        }

        if ($key !== false) {
            $attrs['class'] .= ' ' . $type . '-pos-' . $key;
        }

        if ($type == 'row' && $one_block = $this->rowWithOneBlock($block)) {
            $attrs['class'] .= ' r-'. $one_block->subtype;
        }

        unset($attrs['blockClass'], $attrs['classPrefix']);

        if (empty($block->settings)) {
            return $this->formatAttributes($attrs);
        }

        $styles = new ArrayObject([]);

        foreach ($this->styles as $field => $prop) {
            $this->getPropertyValue($block->settings, $field, $prop, $styles);
        }

        $attrs['style'] .= implode(';', (array)$styles);

        if (!empty($block->settings->class)) {
            $attrs['class'] .= ' ' . $block->settings->class;
        }

        if (Configure::read('Block.paddings') && !empty($block->settings->pt)) {
            $attrs['class'] .= ' g-pt-' . $block->settings->pt;
        }

        if (Configure::read('Block.paddings') && !empty($block->settings->pb)) {
            $attrs['class'] .= ' g-pb-' . $block->settings->pb;
        }

        if (!empty($block->settings->id)) {
            $attrs['id'] = $block->settings->id;
        }

        if ($block instanceof \Block\Model\Entity\Row && !empty($block->settings->fullwidth)) {
            $attrs['class'] .= ' fullwidth';
        }

        if (!empty($block->settings->custom_class)) {
            $attrs['class'] .= ' ' . $block->settings->custom_class;
        }

        if (!empty($block->settings->custom_classes)) {
            $attrs['class'] .= ' ' . str_replace(',', ' ', $block->settings->custom_classes);
        }


        $classes = explode(' ', $attrs['class']);
        $classes = array_unique($classes);
        $attrs['class'] = implode(' ', $classes);

        return $this->formatAttributes($attrs);
    }

    public function rowWithOneBlock($row)
    {
        $count = 0;

        foreach ($row->columns as $column) {
            $count += count($column->blocks);
        }

        if ($count == 1 && !empty(@$row->columns[0]->blocks[0])) {
            return $row->columns[0]->blocks[0];
        }
    }

    public function columns($column)
    {
        if (empty($column->cols) || $column->cols == '0') {
            return 12;
        }

        return $column->cols;
    }

    public function responsiveColumns($column, $defaultSize = 'sm', $names = 'bootstrap')
    {
        $sizes = [
            'xs',
            'sm',
            'md'
        ];

        $class = '';
        $useds = [];

        foreach ($sizes as $size) {
            if (!empty($column->settings->$size)) {
                if ($names == 'bootstrap') {
                    $class .= 'col-' . $size . '-' . $column->settings->$size . ' ';
                } elseif ($names == 'tailwind') {
                    $class .= $size .':col-' . $column->settings->$size . ' ';
                }
                $useds[] = $size;
            }
        }

        if (!in_array($defaultSize, $useds)) {
            if ($names == 'bootstrap') {
                $class .= ' col-' . $defaultSize . '-' . $this->columns($column);
            } elseif ($names == 'tailwind') {
                $class .= ' '. $defaultSize .':col-' . $this->columns($column);
            }
        }

        return $class;
    }

    /**
     * Da formato de string para HTML de un array de atributos, usando StringTemplateTrait  
     * 
     * @param  array $attrs
     * @return string
     */
    public function formatAttributes($attrs)
    {
        return $this->templater()->formatAttributes($attrs);
    }


    public function render($name, $vars = [])
    {
        if (isset($vars['settings'])) {
            $vars['settings'] = (object)$vars['settings'];
        }

        return $this->_View->cell($name . '::display', [
            'block' => new \Block\Model\Entity\Block($vars)
        ]);
    }
}
