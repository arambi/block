<?php
namespace Block\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\Utility\Inflector;

/**
 * Block component
 */
class BlockComponent extends Component
{ 

/**
 * Toma el contenido principal del web
 * Sirve para usar el contenido en titulos o enlaces a redes sociales
 * 
 * @return Entity 
 */
  public function getMainContent()
  {
    $viewVars = $this->_registry->getController()->viewVars;
    $content = false;

    $variable = strtolower( Inflector::singularize( $this->request->controller));

    if( array_key_exists( 'content', $viewVars))
    {
      $content = $viewVars ['content'];
    }
    elseif( array_key_exists( $variable, $viewVars))
    {
      $content = $viewVars [$variable];
    }
    
    return $content;
  }

  public function beforeRender( Event $event)
  {
    if( Configure::read( 'Section.fullwidth') === null)
    {
      $content = $this->getMainContent();
      $section = $this->request->param( 'section');
      $has = false;

      if( ($content && $content->fullwidth) || $section->fullwidth)
      {
        $has = true;
      }

      Configure::write( 'Section.fullwidth', $has);
    }
  }
}
