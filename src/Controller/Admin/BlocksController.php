<?php

namespace Block\Controller\Admin;

use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\Utility\Inflector;
use Cake\Event\EventManager;
use Block\Lib\BlocksRegistry;
use Block\Controller\AppController;
use Manager\Controller\CrudControllerTrait;

/**
 * Blocks Controller
 *
 * @property Block\Model\Table\BlocksTable $Blocks
 */
class BlocksController extends AppController
{
	use CrudControllerTrait;


	/**
	 * Muestra los bloques disponibles para añadir
	 * 
	 * @param string $model El nombre del model
	 */
	public function add($model)
	{
		// Setea todos los tipos de bloques definidos
		$this->CrudTool->addSerialized([
			'actions' => BlocksRegistry::getMany($this->request->data['blocks'], $model)
		]);
	}

	/**
	 * 
	 * @return void
	 */
	private function _beforeCreate()
	{
		$this->_parseModel();
	}

	private function _beforeUpdate()
	{
		$this->_parseModel();
	}

	private function _beforeSerialize()
	{
		$this->_parseModel();
	}


	private function _create()
	{
		$type = $this->_getType();
		$this->Blocks->crud->setContent(['subtype' => $type]);
		$this->Blocks->crud->setContent(['block_type' => BlocksRegistry::get($type)]);
	}



	private function _getType()
	{
		if (isset($this->request->query['subtype'])) {
			return $this->request->query['subtype'];
		}

		if (isset($this->request->data['subtype'])) {
			return $this->request->data['subtype'];
		}

		if (isset($this->request->params['pass'][0]) && !is_numeric($this->request->params['pass'][0])) {
			return $this->request->params['pass'][0];
		}

		$id = $this->getRequest()->getParam('pass.0');

		if (!empty($id)) {
			$block = $this->Blocks->find()
				->where([
					'Blocks.id' => $id
				])
				->first();

			if ($block) {
				return $block->subtype;
			}
		}
	}

	public function setblock()
	{
		$this->CrudTool->setAction('update');
		$this->_parseModel();
	}

	private function _parseModel()
	{
		$type = $this->_getType();
		$blockInfo = BlocksRegistry::get($type);
		$namespace = $blockInfo['className'];
		$block = new $namespace;
		$this->Blocks->addBehavior('Manager.Crudable');
		$block->parse($this->Blocks);

		$event = new Event('Block.Controller.Blocks.parse', $this, [
			$this->Blocks
		]);

		EventManager::instance()->dispatch($event);

		$this->Blocks->setAdmin();
		$this->Blocks->_setGeneral();
	}

	public function blocksInitialize($table)
	{
		$table->removeBehavior('I18nTranslate');
		$table->addBehavior('I18n.I18nTranslate', [
			'table' => ['title', 'antetitle', 'subtitle', 'body', 'video']
		]);

		$table->belongsToMany('CountriesWebsContents', [
			'className' => 'Lacunza.CountriesWebsContents',
			'joinTable' => 'lacunza_web_countries_contents',
			'foreignKey' => 'content_id',
			'targetForeignKey' => 'web_country_id',
		]);

		$table->crud->associations(['CountriesWebsContents']);
	}

	public function blocksBeforeBuild($table)
	{
		$table->crud->addFields([
			'countries_webs_contents' => [
				'type' => 'checkboxes',
				'label' => 'Países',
			],
		]);

		$table->crud->addColumn(['create', 'update'], [
			'key' => 'countries',
			'title' => __d('admin', 'Países'),
			'cols' => 12,
			'box' => [
				[
					'elements' => [
						'countries_webs_contents'
					]
				]
			]
		]);
	}
}
