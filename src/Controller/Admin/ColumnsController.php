<?php
namespace Block\Controller\Admin;

use Manager\Controller\CrudControllerTrait;
use Block\Controller\AppController;

/**
 * Columns Controller
 *
 * @property \Block\Model\Table\ColumnsTable $Columns
 */
class ColumnsController extends AppController
{
    use CrudControllerTrait;
}
