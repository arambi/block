<?php
namespace Block\Controller\Admin;

use Manager\Controller\CrudControllerTrait;
use Block\Controller\AppController;

/**
 * Tabs Controller
 *
 * @property \Block\Model\Table\TabsTable $Tabs
 */
class TabsController extends AppController
{
    use CrudControllerTrait;
}
