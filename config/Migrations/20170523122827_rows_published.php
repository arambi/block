<?php

use Phinx\Migration\AbstractMigration;
use Cake\ORM\TableRegistry;

class RowsPublished extends AbstractMigration
{
   
  public function change()
  {
    $rows = $this->table( 'rows');
    $rows
          ->addColumn( 'published', 'boolean', ['default' => 1, 'null' => false])
          ->addIndex( ['published'])
          ->save();

    $columns = $this->table( 'columns');
    $columns
          ->addColumn( 'published', 'boolean', ['default' => 1, 'null' => false])
          ->addIndex( ['published'])
          ->save();
    
    
    TableRegistry::get( 'Block.Blocks')->updateAll([
      'published' => true
    ], []);
  }
}
