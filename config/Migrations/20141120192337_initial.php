<?php

use Phinx\Migration\AbstractMigration;

class Initial extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
      $this->dropTable( 'rows');
    }
    */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
      $rows = $this->table( 'rows');
      $rows
            ->addColumn( 'model', 'string', array( 'limit' => 16, 'default' => null))
            ->addColumn( 'content_id', 'integer', ['default' => null, 'null' => true])
            ->addColumn( 'position', 'integer', ['limit' => 6, 'null' => false])
            ->addColumn( 'settings', 'text', ['default' => null, 'null' => true])
            ->addColumn( 'salt', 'string', array( 'limit' => 64, 'default' => null))
            ->addColumn( 'created', 'datetime', array('default' => null))
            ->addIndex( ['content_id'])
            ->addIndex( ['position'])
            ->addIndex( ['model'])
            ->save();

      $columns = $this->table( 'columns');
      $columns
            ->addColumn( 'row_id', 'integer', ['default' => null, 'null' => true])
            ->addColumn( 'position', 'integer', ['limit' => 6, 'null' => false])
            ->addColumn( 'settings', 'text', ['default' => null, 'null' => true])
            ->addColumn( 'cols', 'integer', array( 'limit' => 4, 'default' => null))
            ->addColumn( 'salt', 'string', array( 'limit' => 64, 'default' => null))
            ->addColumn( 'created', 'datetime', array('default' => null))
            ->addIndex( ['position'])
            ->addIndex( ['row_id'])
            ->save();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
      if( $this->hasTable( 'contents'))
      {
        $this->query( 'DELETE FROM contents WHERE content_type = "Blocks"');
      }

      if( $this->hasTable( 'i18n'))
      {
        $this->query( 'DELETE FROM i18n WHERE model = "Blocks"');
      }

      $this->dropTable( 'rows');
      $this->dropTable( 'columns');
    }
}