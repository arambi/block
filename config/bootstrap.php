<?php 
use Section\Action\ActionCollection;
 
use Manager\Navigation\NavigationCollection;
 
use User\Auth\Access;


use Cake\Core\Configure;
use Block\Lib\BlocksRegistry;

BlocksRegistry::add( 'text', [
    'title' => __d( 'admin', 'Texto'),
    'icon' => 'fa fa-font',
    'afterAddTarget' => 'parent',
    'inline' => true,
    'unique' => false,
    'deletable' => true,
    'className' => 'Block\\Model\\Block\\TextBlock',
    'cell' => 'Block.Text::display',
    'blockView' => 'Block/blocks/text'
]);

BlocksRegistry::add( 'gallery', [
    'key' => 'gallery',
    'title' => __d( 'admin', 'Galería de fotos'),
    'icon' => 'fa fa-camera',
    'afterAddTarget' => 'parent',
    'inline' => false,
    'unique' => false,
    'deletable' => true,
    'className' => 'Block\\Model\\Block\\GalleryBlock',
    'cell' => 'Block.Gallery::display',
    'blockView' => 'Block/blocks/gallery'
]);

BlocksRegistry::add( 'content', [
    'key' => 'content',
    'title' => __d( 'admin', 'Contenido'),
    'icon' => 'fa fa-camera',
    'afterAddTarget' => 'parent',
    'inline' => true,
    'unique' => true,
    'deletable' => false,   
    'className' => 'Block\\Model\\Block\\ContentBlock',
    'cell' => 'Block.Content::display',
    'blockView' => 'Block/blocks/content'
]);



// Bloque Imagen
BlocksRegistry::add( 'image', [
    'key' => 'image',
    'title' => __d( 'admin', 'Imagen'),
    'icon' => 'fa fa-image',
    'afterAddTarget' => 'parent',
    'inline' => false,
    'unique' => false,
    'deletable' => true,   
    'className' => 'Block\\Model\\Block\\ImageBlock',
    'cell' => 'Block.Image::display',
    'blockView' => 'Block/blocks/Image'
]);

// Bloque Mapa simple
BlocksRegistry::add( 'simplemap', [
    'key' => 'simplemap',
    'title' => __d( 'admin', 'Mapa simple'),
    'icon' => 'fa fa-map-marker',
    'afterAddTarget' => 'parent',
    'inline' => false,
    'unique' => false,
    'deletable' => true,   
    'className' => 'Block\\Model\\Block\\SimplemapBlock',
    'cell' => 'Block.Simplemap::display',
    'blockView' => 'Block/blocks/simplemap'
]);

/*
BlocksRegistry::add( 'banner', [
    'key' => 'banner',
    'title' => __d( 'admin', 'Banner'),
    'icon' => 'fa fa-th-large',
    'afterAddTarget' => 'parent',
    'inline' => false,
    'unique' => true,
    'deletable' => true,   
    'className' => 'Block\\Model\\Block\\BannerBlock',
    'cell' => 'Block.Banner::display',
    'blockView' => 'Block/blocks/banner'
]);

*/


Access::add( 'blocks', [
  'name' => 'Bloques  ',
  'options' => [
    'edit' => [
      'name' => 'Edición',
      'nodes' => [
        [
          'prefix' => 'admin',
          'plugin' => 'Block',
          'controller' => 'Rows',
          'action' => 'index',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Block',
          'controller' => 'Rows',
          'action' => 'add',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Block',
          'controller' => 'Rows',
          'action' => 'create',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Block',
          'controller' => 'Rows',
          'action' => 'update',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Block',
          'controller' => 'Rows',
          'action' => 'serialize',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Block',
          'controller' => 'Rows',
          'action' => 'delete',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Block',
          'controller' => 'Columns',
          'action' => 'index',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Block',
          'controller' => 'Columns',
          'action' => 'add',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Block',
          'controller' => 'Columns',
          'action' => 'create',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Block',
          'controller' => 'Columns',
          'action' => 'update',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Block',
          'controller' => 'Columns',
          'action' => 'serialize',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Block',
          'controller' => 'Columns',
          'action' => 'delete',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Block',
          'controller' => 'Blocks',
          'action' => 'serialize',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Block',
          'controller' => 'Blocks',
          'action' => 'add',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Block',
          'controller' => 'Blocks',
          'action' => 'create',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Block',
          'controller' => 'Blocks',
          'action' => 'update',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Block',
          'controller' => 'Blocks',
          'action' => 'field',
        ],
      ]
    ]
  ]
]);



















// Bloque Pestanas
BlocksRegistry::add( 'tabs', [
    'key' => 'tabs',
    'title' => __d( 'admin', 'Pestanas'),
    'icon' => 'fa fa-ellipsis-h',
    'afterAddTarget' => 'parent',
    'inline' => false,
    'unique' => true,
    'deletable' => true,   
    'className' => 'Block\\Model\\Block\\TabsBlock',
    'cell' => 'Block.Tabs::display',
    'blockView' => 'Block/blocks/tabs'
]);




Access::add( 'tabs', [
  'name' => 'Pestanas',
  'options' => [
    'edit' => [
      'name' => 'Edición',
      'nodes' => [
        [
          'prefix' => 'admin',
          'plugin' => 'Block',
          'controller' => 'Tabs',
          'action' => 'index',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Block',
          'controller' => 'Tabs',
          'action' => 'add',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Block',
          'controller' => 'Tabs',
          'action' => 'create',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Block',
          'controller' => 'Tabs',
          'action' => 'update',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Block',
          'controller' => 'Tabs',
          'action' => 'delete',
        ]
      ]
    ]
  ]
]);







// Bloque Mailchimp
BlocksRegistry::add( 'mailchimp', [
    'key' => 'mailchimp',
    'title' => __d( 'admin', 'Mailchimp'),
    'icon' => 'fa fa-envelope-o',
    'afterAddTarget' => 'parent',
    'inline' => false,
    'unique' => true,
    'deletable' => true,   
    'className' => 'Block\\Model\\Block\\MailchimpBlock',
    'cell' => 'Block.Mailchimp::display',
    'blockView' => 'Block/blocks/mailchimp'
]);





// Bloque Html
BlocksRegistry::add( 'html', [
    'key' => 'html',
    'title' => __d( 'admin', 'Código Html'),
    'icon' => 'fa fa-code',
    'afterAddTarget' => 'parent',
    'inline' => false,
    'unique' => false,
    'deletable' => true,   
    'className' => 'Block\\Model\\Block\\HtmlBlock',
    'cell' => 'Block.Html::display',
    'blockView' => 'Block/blocks/html'
]);





// Bloque Bloque existente
BlocksRegistry::add( 'reference', [
    'key' => 'reference',
    'title' => __d( 'admin', 'Bloque existente'),
    'icon' => 'fa fa-square-o',
    'afterAddTarget' => 'parent',
    'inline' => false,
    'unique' => false,
    'deletable' => true,   
    'className' => 'Block\\Model\\Block\\ReferenceBlock',
    'cell' => 'Block.Reference::display',
    'blockView' => 'Block/blocks/reference'
]);





// Bloque Contenido Social
BlocksRegistry::add( 'social_content', [
    'key' => 'SocialContent',
    'title' => __d( 'admin', 'Contenido Social'),
    'icon' => 'fa fa-users',
    'afterAddTarget' => 'parent',
    'inline' => false,
    'unique' => false,
    'deletable' => true,   
    'className' => 'Block\\Model\\Block\\SocialContentBlock',
    'cell' => 'Block.SocialContent::display',
    'blockView' => 'Block/blocks/social_content'
]);





// Bloque Adjuntos
BlocksRegistry::add( 'documents', [
    'key' => 'documents',
    'title' => __d( 'admin', 'Adjuntos'),
    'icon' => 'fa fa-file',
    'afterAddTarget' => 'parent',
    'inline' => false,
    'unique' => false,
    'deletable' => true,   
    'className' => 'Block\\Model\\Block\\DocumentsBlock',
    'cell' => 'Block.Documents::display',
    'blockView' => 'Block/blocks/documents'
]);





// Bloque Video
BlocksRegistry::add( 'video', [
    'key' => 'video',
    'title' => __d( 'admin', 'Video'),
    'icon' => 'fa fa-video-camera',
    'afterAddTarget' => 'parent',
    'inline' => false,
    'unique' => false,
    'deletable' => true,   
    'className' => 'Block\\Model\\Block\\VideoBlock',
    'cell' => 'Block.Video::display',
    'blockView' => 'Block/blocks/video'
]);





// Bloque Enlaces
BlocksRegistry::add( 'links', [
    'key' => 'links',
    'title' => __d( 'admin', 'Enlaces'),
    'icon' => 'fa fa-link',
    'afterAddTarget' => 'parent',
    'inline' => false,
    'unique' => false,
    'deletable' => true,   
    'className' => 'Block\\Model\\Block\\LinksBlock',
    'cell' => 'Block.Links::display',
    'blockView' => 'Block/blocks/links'
]);
