;(function(){
  /**
   * BotÃ³n de aÃ±adir fila
   * La acciÃ³n aÃ±ade una fila y un bloque
   */
  
  
  


  /**
   * Devuelve un array con los keys disponibles para la configuraciÃ³n dada
   * 
   * @param  object config
   * @return array
   */
  function getBlocks( config) {
    var result = [];
    for( var property in config) {
      result.push( property);
    }

    return result;
  }

/**
 * Devuelve un array con los keys de los bloques que sean Ãºnicos para la configuraciÃ³n dada
 * 
 * @param  object config
 * @return array
 */
  function getBlockUniques( config) {
    var result = [];
    for( var property in config) {
      if( config [property].unique) {
        result.push( property);
      }
    }

    return result;
  }

/**
 * Devuelve un array con los keys de los bloques que se estÃ©n usando en la vista
 * 
 * @param  object rows
 * @return array
 */
  function getUsedBlocks( rows){
    var result = [];

    for( var i = 0; i < rows.length; i++){
      for( var x = 0; x < rows[i].columns.length; x++){
        for( var z = 0; z < rows[i].columns[x].blocks.length; z++){
          result.push( rows[i].columns[x].blocks[z].subtype);
        }
      }
        
    }

    return result;
  }

/**
 * Devuelve un array con los bloques que se pueden usar
 * 
 * @param  object config
 * @param  object rows
 * @return array
 */
  function getAvailablesBlocks( config, rows){
    var availables = getBlocks( config);
    var uniques = getBlockUniques( config);
    var useds = getUsedBlocks( rows);
    var result = [];

    for( var i = 0; i < availables.length; i++){
      if( uniques.indexOf( availables[i]) == -1 || useds.indexOf( availables[i]) == -1){
        result.push( availables[i]);
      }
    }

    return result;
  }


function cfAddRow( $modal, $http, $timeout){
    return {
      restrict: 'A',
      scope: '=',
      link: function( scope, element, attrs){
        if( !scope.sortableRow){
          scope.sortableRow = {
            handle: '.handle-row',
            items: '.row-container',
            axis: 'y',
            distance: 20            
          }
        }

        if( !scope.sortableBlock){
          scope.sortableBlock = {
            connectWith: '.column-container-' + scope.crudConfig.blockWrapper, 
            handle: '.handle-block',
            items: '.block-container',
            distance: 50
          }
        }

        if( !scope.sortableColumn){
          scope.sortableColumn = {
            handle: '.handle-column',
            items: '.column-container',
            // connectWith: '.row-container-' + scope.crudConfig.blockWrapper, 
            distance: 20            
          }
        }
      
        // Modal instance
        var modalCtrl = function( $scope, $modalInstance) {
          // Toma un array con los bloques Ãºnicos disponibles en este contenido
          var blocks = getAvailablesBlocks( scope.crudConfig.blocks, scope.content.rows);
          $http.post( '/admin/block/blocks/add/' + scope.crudConfig.model.alias + '.json', {
            blocks: blocks
          })
            .success(function( result){
              // AquÃ­ llegan los bloques
              $scope.data = result;

            })
            .error( function( result){
              console.log( result)
            })

            $scope.cancel = function () {
              $modalInstance.dismiss( 'cancel');
            };

            $scope.addBlock = function( action){
              $http.get( '/admin/block/blocks/create/' + action.key + '.json')
                .success( function( result){
                  // Es un contenido inline, que se coloca directamente en los bloques
                  if( action.inline){
                    $http.post( '/admin/block/blocks/create.json', result.content)
                      .success( function( result){
                        // Añade el block 
                        
                        scope.content.rows.splice( attrs.index, 0, {
                          published: true,
                          columns: [{
                            published: true,
                            settings: {
                              cols: 12
                            },
                            blocks: [result.content]
                          }]
                          // blocks: [result.content]
                        });
                        $modalInstance.dismiss( 'cancel');
                      })
                      .error( function( result){
                        console.log( result)
                      })

                    /*
                    scope.content.rows.splice( attrs.index, 0, {
                      blocks: [result.content]
                    });
                    */
                    $modalInstance.dismiss( 'cancel');
                  } 
                  // es un contenido que se va a editar antes de colocar en los bloques
                  else {
                    $scope.data = result;
                  }
                })
            }

            $scope.send = function( params, data){
              $http.post( params.url, data)
                .success( function( result){
                  $scope.data = result;
                    console.log( result);
                  
                  // Añade el block 
                  scope.content.rows.splice( attrs.index, 0, {
                    published: true,
                    columns: [{
                      published: true,
                      settings: {
                        cols: 12
                      },
                      blocks: [result.content]
                    }]
                  });
                  $modalInstance.dismiss( 'cancel');
                })
                .error( function( result){
                  console.log( result)
                })

              /*
              scope.content.rows.splice( attrs.index, 0, {
                blocks: [data]
              });
              */
              $modalInstance.dismiss( 'cancel');
            }
          
        };

        element.click(function(){
          $modal.open({
            templateUrl: '/admin/manager/pages/modal',
            controller: modalCtrl,
            size: 'lg'
          });
          return false;
        })
      }
    }
  }


/**
   * Botón de añadir block
   */
  function cfAddBlock( $modal, $http, $timeout, cfCalculateRows, cfSpinner){
    return {
      restrict: 'A',
      scope: '=',
      link: function( scope, element, attrs){

        // Calcula el total de columnas que tiene la fila
        // y otorga al nuevo bloque la anchura restante
        scope.$parent.content.rows[attrs.rowIndex].totalCols = cfCalculateRows.calcule( scope.$parent.content.rows[attrs.rowIndex]);

        // Modal instance
        var modalCtrl = function( $scope, $modalInstance) {
          // Toma un array con los bloques Ãºnicos disponibles en este contenido
          var blocks = getAvailablesBlocks( scope.crudConfig.blocks, scope.content.rows);

          $http.post( '/admin/block/blocks/add/' + scope.crudConfig.model.alias + '.json', {
            blocks: blocks
          })
            .success(function( result){
              $scope.data = result;
            })
            .error( function( result){
              console.log( result)
            })

            $scope.cancel = function () {
              $modalInstance.dismiss( 'cancel');
            };

            // Acción de añadir bloque
            $scope.addBlock = function( action){
              $http.get( '/admin/block/blocks/create/' + action.key + '.json')
                .success( function( result){
                  result.content.cols = 12 - scope.$parent.content.rows[attrs.rowIndex].totalCols;
                  if( action.inline){
                    scope.content.rows[attrs.rowIndex].columns[attrs.columnIndex].blocks.splice( attrs.blockIndex, 0, result.content)
                    $modalInstance.dismiss( 'cancel');
                  } else {
                    $scope.data = result;
                  }
                })
            }

            // Coloca el block en la vista
            $scope.send = function( params, data){
              cfSpinner.enable();
              $http.post( params.url, data)
                .success( function( result){
                  $scope.data = result;
                  scope.block = result.content;
                  scope.content.rows[attrs.rowIndex].columns[attrs.columnIndex].blocks.splice( attrs.blockIndex, 0, result.content)
                  scope.$parent.content.rows[attrs.rowIndex].totalCols = cfCalculateRows.calcule( scope.$parent.content.rows[attrs.rowIndex]);
                  // scope.$parent.$parent.$parent.content.rows [scope.$parent.$index].blocks [scope.$index] = result.content;
                  $modalInstance.dismiss( 'cancel');
                  cfSpinner.disable();
                })
                .error( function( result){
                  console.log( result)
                })

              $modalInstance.dismiss( 'cancel');
            }
          
        };

        element.click(function(){
          $modal.open({
            templateUrl: '/admin/manager/pages/modal',
            controller: modalCtrl,
            size: 'lg'
          });
          return false;
        })
      }
    }
  }

  /**
   * BotÃ³n de ediciÃ³n de bloque
   */
  function cfEditBlock( $modal, $http, $timeout){
    return {
      restrict: 'A',
      scope: '=',
      link: function( scope, element, attrs){

        // Modal instance
        var modalCtrl = function( $scope, $modalInstance) {
            // Se hace la peticiÃ³n de la informaciÃ³n del bloque pero se setea la informaciÃ³n ya disponible en la vista padre
            $http.get( '/admin/block/blocks/update/' + scope.block.id + '.json?subtype=' + scope.block.subtype)
              .success(function( result){
                  $scope.data = result;
              })
              .error( function( result){
                console.log( result)
              })
            
            $scope.cancel = function () {
              $modalInstance.dismiss( 'cancel');
            };

            $scope.send = function( params, data){
              $http.post( params.url, data)
                .success( function( result){
                  $scope.data = result;
                  scope.block = result.content;

                  if (scope.$parent.$parent.$parent.content.rows [scope.$parent.$parent.$index].columns[scope.$parent.$index]) {
                    scope.$parent.$parent.$parent.content.rows [scope.$parent.$parent.$index].columns[scope.$parent.$index].blocks [scope.$index] = result.content;
                  }
                  $modalInstance.dismiss( 'cancel');
                })
                .error( function( result){
                  console.log( result)
                })
            }
            
        };

        element.click(function(){
          $modal.open({
            templateUrl: '/admin/manager/pages/modal',
            controller: modalCtrl,
            size: 'lg'
          });
          return false;
        })
      }
    }
  }

  function cfRemoveBlock() {
    return {
      restrict: 'A',
      scope: '=',
      link: function( scope, element, attrs){
        element.click(function(){
          scope.$parent.content.rows[attrs.rowIndex].blocks.splice( attrs.blockIndex, 1);

          if( scope.$parent.content.rows[attrs.rowIndex].blocks.length == 0){
            scope.$parent.content.rows.splice( attrs.rowIndex, 1);
          }
          scope.$parent.$apply();
          return false;
        })
      }
    }
  }

  function cfChangeColsBlock( cfCalculateRows) {
    return {
      restrict: 'A',
      scope: '=',
      link: function( scope, element, attrs){
        element.click(function(){
          if( attrs.action == 'increase'){
            scope.block.cols++;
          } else {
            scope.block.cols--;
          }

          if( scope.block.cols < 1){
            scope.block.cols = 1;
          }
          
          scope.$apply();
          var totalCols = cfCalculateRows.calcule( scope.$parent.content.rows[attrs.rowIndex]);

          if( totalCols > 12){
            scope.block.cols--;
          }

          scope.$apply();
          scope.$parent.content.rows[attrs.rowIndex].totalCols = cfCalculateRows.calcule( scope.$parent.content.rows[attrs.rowIndex]);

          return false;
        })
      }
    }
  }

  /**
   * Directiva para el cambio de tamaño de los bloques, usando jQueryUI Resizable
   */
  function resizable( cfCalculateRows, $rootScope){
    
    /**
     * Toma la anchura del bloque en píxeles
     * @param  element El bloque
     * @return float
     */
    function getWidth( element){
      return element.parent().width() / 6;
    }

    /**
     * Devuelve el tamaño total de columnas de una fila
     * @param  array blocks Array de bloques
     * @return integer
     */
    function getRowCols( columns){
      var total = 0;

      for( var i = 0; i < columns.length; i++){
        total = total + columns[i].cols;
      }
      return total;
    }

    
    return {
        restrict: 'A',
        scope: '=',
        require: 'ngModel',
        link: function postLink( scope, element, attrs, ngModel) {
            var originalSize = false;

            // Añade jQueryUI Resizable al elemento
            element.resizable({
                grid: getWidth( element),

                resize: function( event, ui){

                    var cols = scope.column.cols;

                    if( !originalSize){
                      originalSize = ui.originalSize.width;
                    }

                    // Se amplia la columna
                    if( ui.size.width > originalSize){
                      cols = cols + 2;
                    } 
                    // Se disminuye la columna
                    else { 
                      cols = cols - 2;
                    }

                    originalSize = ui.size.width;

                    if( cols < 2){
                      cols = 2; 
                    }

                    scope.column.cols = cols;
                    scope.$apply();

                    // El total de la fila ha llegado a más de 12
                    if( getRowCols( scope.$eval( attrs.ngModel)) > 12){

                      var columns = scope.$eval( attrs.ngModel);

                      if( columns[scope.$index + 1] && columns[scope.$index + 1].cols > 2){
                        columns[scope.$index + 1].cols = columns[scope.$index + 1].cols - 2;
                        ngModel.$setViewValue( columns);
                      }
                      else {
                        scope.column.cols = scope.column.cols - 2;
                        scope.$apply();
                        ui.element.removeAttr( 'style');  
                        return false;
                      }
  
                    }

                    ui.element.removeAttr( 'style');  
                },
                handles: 'e'
            });
        }
    };
  }

/**
 * [cfBlocks description]
 * @return {[type]} [description]
 */
  function cfColumns( $timeout){
    return {
        restrict: 'A',
        scope: '=',
        require: 'ngModel',
        link: function( scope, element, attrs, ngModel){
          scope.$watch( attrs['ngModel'], function( newValue){

            var columns = scope.$eval( attrs ['ngModel']);
            if( columns.length == 0){
              scope.$parent.content.rows.splice( scope.$index, 1);
              $timeout(function(){
                scope.$apply();
              });

              return;
            }

            for( var i = 0; i < columns.length; i++){
              columns[i].position = (i + 1);
            }
            
            $timeout(function(){
              scope.$apply();
            });

          }, true);

        }
    };
  }

  function cfBlocks( $timeout){
    return {
        restrict: 'A',
        scope: '=',
        require: 'ngModel',
        link: function( scope, element, attrs, ngModel){
          scope.$watch( attrs['ngModel'], function( newValue){

            var blocks = scope.$eval( attrs ['ngModel']);

            for( var i = 0; i < blocks.length; i++){
              blocks[i].position = (i + 1);
            }
            
            $timeout(function(){
              scope.$apply();
            });

          }, true);

        }
    };
  }


  function cfRowLayout( $timeout){
    return {
      restrict: 'A',
      scope: '=',
      link: function( scope, element, attrs, ngModel){
        element.click(function(){
          var cols = scope.row.columns.length;
          
          if( cols < attrs.cfRowLayout){
            scope.row.columns[0].cols = 6;
            scope.row.columns.splice( 1, 0, {
              blocks: [],
              published: true,
              settings: {
                cols: 6
              }
            });
            $timeout(function(){
              scope.$apply();
            })
          }
        })
      }
    }
  }

  function cfColumnsLayout( $timeout){
    function calcule( data){
      var prop = data.split(" + ");

      var cols = [];
      for( var i = 0; i < prop.length; i++){
        if( prop[i].indexOf('/') !== false){
          var nums = prop[i].split('/');
          if( !isNaN(nums[0]) && !isNaN( nums[1]) && nums[0] <= nums[1]){
            var col = Math.round((nums[0] * 12) / nums[1]);
            if( col > 0 && col <= 12){
              cols.push( col);
            }
          }
          
        }
      }

      return cols;
    }

    return {
      restrict: 'A',
      scope: '=',
      require: 'ngModel',
      link: function( scope, element, attrs, ngModel){
        element[0].setAttribute( 'title', attrs.cfColumnsLayout);
        element.click(function(){
          var cols = calcule( attrs.cfColumnsLayout);
          var row = ngModel.$viewValue;

          // Hay más columnas que antes
          if( cols.length > row.columns.length){
            var diff = cols.length - row.columns.length;
            for( var i = 0; i < diff; i++){
              row.columns.push({
                published: true,
                settings:{

                },
                blocks: []
              });
            }
          } 
          // Se han eliminado columnas
          else if( cols.length < row.columns.length){
            var diff = row.columns.length - cols.length;
            var current = row.columns.length - 1;

            for( i = 0; i < diff; i++){
              // Copia los bloques a la columna anterior
              if( row.columns[current].blocks){
                for( var z = 0; z < row.columns[current].blocks.length; z++){
                  row.columns[0].blocks.push( row.columns[current].blocks[z]);
                }
              }
              row.columns.splice( current, 1);
              current--;
            }
          }

          for( var i = 0; i < row.columns.length; i++){
            row.columns[i].cols = cols[i];
          }

          scope.$apply();
        });



        
      }
    }
  }

  /**
   *
   * Pass all functions into module
   */
  angular
      .module('admin')
      .directive( 'cfAddRow', cfAddRow)
      .directive( 'cfAddBlock', cfAddBlock)
      .directive( 'cfRemoveBlock', cfRemoveBlock)
      .directive( 'cfChangeColsBlock', cfChangeColsBlock)
      .directive( 'cfEditBlock', cfEditBlock)
      .directive( 'resizable', resizable)
      .directive( 'cfBlocks', cfBlocks)
      .directive( 'cfColumns', cfColumns)
      .directive( 'cfRowLayout', cfRowLayout)
      .directive( 'cfColumnsLayout', cfColumnsLayout)
  ;
})();