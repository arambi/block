
function cfCalculateRows(){

  function calcule( row){
    var totalCols = 0;

    for (var i = row.columns.length - 1; i >= 0; i--) {
      totalCols += row.columns[i].cols;
    };

    return totalCols;
  }

  this.calcule = calcule;
}

angular
    .module( 'admin')
    .service( 'cfCalculateRows', cfCalculateRows)
    