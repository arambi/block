# Blocks Plugin



## Configuración y creación de nuevos bloques

Por cada bloque es necesario configurar, dentro del fichero plugins/Block/config/blocks.php, un array que tendrá como key el nombre computerizado del bloque y los siguientes pair/values:

* `key`: El mismo key del array
* `title`: El nombre humano que debe ir dentro de la función `__d( 'admin', 'Título')`
* `icon`: El CSS class del icono
* `inline`: Indica si la edición del bloque se hace en la propia edición (`true`) o se hace en una ventana modal (`false`)
* `unique`: Si es `true`, el bloque será único en cada fila